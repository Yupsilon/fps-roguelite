﻿using UnityEngine;

public class property_GUI  
{
	//		MAIN MENU
	public GUIStyle MainMenuBG;
	public GUIStyle MainMenuUI;
	public GUIStyle MainMenuBU;

	public float Multiplier=1;
	public property_GUI()
	{
		Multiplier = Mathf.Max (1600f / Screen.width, 900f / Screen.height) / 2f;
		reDraw ();Rescale (Multiplier);
	} public void reDraw(){

		MainMenuBG = new GUIStyle ();
		MainMenuBG.normal.background = Resources.Load ("GUI/MainMenuBG") as Texture2D;
		MainMenuBG.normal.background.filterMode = FilterMode.Point;
		MainMenuUI = new GUIStyle ();
		MainMenuUI.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		MainMenuUI.normal.background.filterMode = FilterMode.Point;		
		MainMenuBU = new GUIStyle ();
		MainMenuBU.normal.background = Resources.Load ("GUI/MainMenuBU") as Texture2D;
		MainMenuBU.normal.background.filterMode = FilterMode.Point;
		MainMenuBU.alignment = TextAnchor.MiddleCenter;
		MainMenuBU.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		MainMenuBU.normal.textColor = Color.black;

		/*SubMenuUI = new GUIStyle ();
		SubMenuUI.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SubMenuUI.normal.background.filterMode = FilterMode.Point;
		SubMenuUI.alignment = TextAnchor.MiddleCenter;		
		SubMenuBU = new GUIStyle ();		
		SubMenuBU.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SubMenuBU.normal.background.filterMode = FilterMode.Point;
		SubMenuBU.alignment = TextAnchor.MiddleCenter;		
		SubMenuBG = new GUIStyle ();	
		SubMenuBG.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SubMenuBG.normal.background.filterMode = FilterMode.Point;
		SubMenuBG.alignment = TextAnchor.MiddleCenter;

		SubMenuMapLA = new GUIStyle ();	

		SubMenuMapBU = new GUIStyle ();		
		SubMenuMapBU.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SubMenuMapBU.normal.background.filterMode = FilterMode.Point;
		SubMenuMapBU.alignment = TextAnchor.MiddleCenter;		
		SubMenuPlayerBU = new GUIStyle ();		
		SubMenuPlayerBU.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SubMenuPlayerBU.normal.background.filterMode = FilterMode.Point;
		SubMenuPlayerBU.alignment = TextAnchor.MiddleCenter;		
		SubMenuPlayerNM = new GUIStyle ();		
		SubMenuPlayerNM.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SubMenuPlayerNM.normal.background.filterMode = FilterMode.Point;
		SubMenuPlayerNM.alignment = TextAnchor.MiddleCenter;		
		SubMenuPlayerUI = new GUIStyle ();	
		SubMenuPlayerUI.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SubMenuPlayerUI.normal.background.filterMode = FilterMode.Point;
		SubMenuPlayerUI.alignment = TextAnchor.MiddleCenter;

		SelectionMenuBG = new GUIStyle ();
		SelectionMenuBG.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SelectionMenuBG.normal.background.filterMode = FilterMode.Point;
		SelectionMenuBG.alignment = TextAnchor.MiddleCenter;
		SelectionMenuUI = new GUIStyle ();
		SelectionMenuUI.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SelectionMenuUI.normal.background.filterMode = FilterMode.Point;
		SelectionMenuUI.alignment = TextAnchor.MiddleCenter;
		SelectionMenuTT = new GUIStyle ();
		SelectionMenuTT.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SelectionMenuTT.normal.background.filterMode = FilterMode.Point;
		SelectionMenuTT.alignment = TextAnchor.MiddleCenter;
		SelectionMenuBU = new GUIStyle ();
		SelectionMenuBU.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SelectionMenuBU.normal.background.filterMode = FilterMode.Point;
		SelectionMenuBU.alignment = TextAnchor.MiddleCenter;
		SelectionMenuSB = new GUIStyle ();
		SelectionMenuSB.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SelectionMenuSB.normal.background.filterMode = FilterMode.Point;
		SelectionMenuSB.alignment = TextAnchor.MiddleCenter;

		SelectionMenuWorldNA = new GUIStyle ();
		SelectionMenuWorldNA.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SelectionMenuWorldNA.normal.background.filterMode = FilterMode.Point;
		SelectionMenuWorldNA.alignment = TextAnchor.MiddleCenter;
		SelectionMenuWorldBA = new GUIStyle ();
		SelectionMenuWorldBA.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SelectionMenuWorldBA.normal.background.filterMode = FilterMode.Point;
		SelectionMenuWorldBA.alignment = TextAnchor.MiddleCenter;
		SelectionMenuWorldBB = new GUIStyle ();
		SelectionMenuWorldBB.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		SelectionMenuWorldBB.normal.background.filterMode = FilterMode.Point;
		SelectionMenuWorldBB.alignment = TextAnchor.MiddleCenter;

		SearchListIndexSelected = new GUIStyle ();
		SearchListIndexSelected.normal.background=Resources.Load ("GUI/MainMenu/CampaignMenuButtona") as Texture2D;
		SearchListIndexSelected.hover.background=Resources.Load ("GUI/MainMenu/CampaignMenuButtonb") as Texture2D;
		SearchListIndexSelected.active.background=Resources.Load ("GUI/MainMenu/CampaignMenuButtond") as Texture2D;
		SearchListIndexSelected.font = Resources.Load ("GUI/Fonts/MainFont") as Font;
		SearchListIndexSelected.alignment = TextAnchor.MiddleLeft;		
		SearchListIndexSelected.normal.textColor=Color.white;
		SearchListIndexSelected.hover.textColor=Color.white;
		SearchListIndexSelected.active.textColor=Color.white;

		SearchListIndex = new GUIStyle ();
		SearchListIndex.normal.background=Resources.Load ("GUI/MainMenu/CampaignMenuButtonc") as Texture2D;
		SearchListIndex.font = Resources.Load ("GUI/Fonts/MainFont") as Font;
		SearchListIndex.alignment = TextAnchor.MiddleLeft;		
		SearchListIndex.normal.textColor=Color.white;

		SkullScrollUpButtonOff = new GUIStyle ();
		SkullScrollUpButtonOff.normal.background=Resources.Load ("GUI/MainMenu/OverlayButtonUpc") as Texture2D;

		SkullScrollDownButtonOff = new GUIStyle ();
		SkullScrollDownButtonOff.normal.background=Resources.Load ("GUI/MainMenu/OverlayButtonDownc") as Texture2D;

		SkullScrollUpButton = new GUIStyle ();
		SkullScrollUpButton.normal.background=Resources.Load ("GUI/MainMenu/OverlayButtonUpb") as Texture2D;
		SkullScrollUpButton.hover.background=Resources.Load ("GUI/MainMenu/OverlayButtonUpa") as Texture2D;
		SkullScrollUpButton.active.background=Resources.Load ("GUI/MainMenu/OverlayButtonUpc") as Texture2D;

		SkullScrollDownButton = new GUIStyle ();
		SkullScrollDownButton.normal.background=Resources.Load ("GUI/MainMenu/OverlayButtonDownb") as Texture2D;
		SkullScrollDownButton.hover.background=Resources.Load ("GUI/MainMenu/OverlayButtonDowna") as Texture2D;
		SkullScrollDownButton.active.background=Resources.Load ("GUI/MainMenu/OverlayButtonDownc") as Texture2D;

		SkullScroll = new GUIStyle ();
		SkullScroll.normal.background=Resources.Load ("GUI/MainMenu/ScrollBarBG") as Texture2D;

		SkullScrollThumb = new GUIStyle ();
		SkullScrollThumb.normal.background=Resources.Load ("GUI/MainMenu/OverlaySlidebarSkull") as Texture2D;

		StoneOverlay = new GUIStyle ();
		StoneOverlay.normal.background=Resources.Load ("GUI/MainMenu/StoneOverlay") as Texture2D;
		StoneOverlay.border = new RectOffset(8,8,8,8);
		StoneOverlay.overflow = new RectOffset(8,8,8,8);
		StoneOverlay.font = Resources.GetBuiltinResource<Font> ("Arial.ttf");//Resources.Load ("GUI/Fonts/MainFont.tff") as Font;
		StoneOverlay.alignment = TextAnchor.MiddleCenter;		
		StoneOverlay.normal.textColor=Color.white;

		EditorLabel = new GUIStyle ();
		EditorLabel.normal.background=Resources.Load ("GUI/MainMenu/Editor256") as Texture2D;
		EditorLabel.font = Resources.GetBuiltinResource<Font> ("Arial.ttf");//Resources.Load ("GUI/Fonts/MainFont.tff") as Font;
		EditorLabel.alignment = TextAnchor.UpperLeft;		
		EditorLabel.normal.textColor=Color.white;

		MainMenuLabel = new GUIStyle ();
		MainMenuLabel.font =Resources.GetBuiltinResource<Font> ("Arial.ttf");// Resources.Load ("GUI/Fonts/MainFont.tff") as Font;
		MainMenuLabel.alignment = TextAnchor.MiddleCenter;		
		MainMenuLabel.normal.textColor=Color.white;

		MainMenuButton = new GUIStyle ();
		MainMenuButton.font = Resources.GetBuiltinResource<Font> ("Arial.ttf");//Resources.Load ("GUI/Fonts/BigFont.tff") as Font;
		MainMenuButton.alignment = TextAnchor.UpperCenter;		
		MainMenuButton.normal.textColor=new Color (222/255f, 174/255f, 140/255f);

		MainMenuTitle = new GUIStyle ();
		MainMenuTitle.font = Resources.GetBuiltinResource<Font> ("Arial.ttf");//Resources.Load ("GUI/Fonts/MainFont.tff") as Font;
		MainMenuTitle.alignment = TextAnchor.UpperLeft;		
		MainMenuTitle.normal.textColor=Color.white;

		HeroName = new GUIStyle ();
		HeroName.font = Resources.GetBuiltinResource<Font> ("Arial.ttf");//Resources.Load ("GUI/Fonts/BigFont.tff") as Font;
		HeroName.alignment = TextAnchor.UpperCenter;		
		HeroName.normal.textColor=Color.white;

		Droplist153 = new GUIStyle ();
		Droplist153.normal.background=Resources.Load ("GUI/MainMenu/Droplist153a") as Texture2D;
		Droplist153.hover.background=Resources.Load ("GUI/MainMenu/Droplist153b") as Texture2D;
		Droplist153.active.background=Resources.Load ("GUI/MainMenu/Droplist153d") as Texture2D;
		Droplist153.font = Resources.Load ("GUI/Fonts/MainFont.tff") as Font;
		Droplist153.alignment = TextAnchor.MiddleLeft;		
		Droplist153.normal.textColor=Color.white;
		Droplist153.hover.textColor=Color.white;
		Droplist153.active.textColor=Color.white;

		Droplist153Closed = new GUIStyle ();
		Droplist153Closed.normal.background=Resources.Load ("GUI/MainMenu/Droplist153c") as Texture2D;
		Droplist153Closed.font = Resources.Load ("GUI/Fonts/MainFont.tff") as Font;
		Droplist153Closed.alignment = TextAnchor.MiddleLeft;		
		Droplist153Closed.normal.textColor=Color.white;


		SubMenuCheckBoxTrue = new GUIStyle ();
		SubMenuCheckBoxTrue.normal.background=Resources.Load ("GUI/MainMenu/CheckBoxa") as Texture2D;
		SubMenuCheckBoxTrue.hover.background=Resources.Load ("GUI/MainMenu/CheckBoxb") as Texture2D;
		SubMenuCheckBoxTrue.active.background=Resources.Load ("GUI/MainMenu/CheckBoxd") as Texture2D;

		SubMenuCheckBox = new GUIStyle ();
		SubMenuCheckBox.normal.background=Resources.Load ("GUI/MainMenu/CheckBoxe") as Texture2D;
		SubMenuCheckBox.hover.background=Resources.Load ("GUI/MainMenu/CheckBoxf") as Texture2D;
		SubMenuCheckBox.active.background=Resources.Load ("GUI/MainMenu/CheckBoxh") as Texture2D;

		SubMenuCheckBoxOff = new GUIStyle ();
		SubMenuCheckBoxOff.normal.background=Resources.Load ("GUI/MainMenu/CheckBoxg") as Texture2D;

		OverlayBG = new GUIStyle ();
		OverlayBG.normal.background=Resources.Load ("GUI/MainMenu/OverlayBackground") as Texture2D;
		OverlayBG.border = new RectOffset(4,4,4,4);
		OverlayBG.overflow = new RectOffset(4,4,4,4);

		Button200 = new GUIStyle ();
		Button200.normal.background=Resources.Load ("GUI/MainMenu/Button200a") as Texture2D;
		Button200.hover.background=Resources.Load ("GUI/MainMenu/Button200b") as Texture2D;
		Button200.active.background=Resources.Load ("GUI/MainMenu/Button200d") as Texture2D;
		Button200.font = Resources.Load ("GUI/Fonts/BigFont.tff") as Font;
		Button200.alignment = TextAnchor.MiddleCenter;		
		Button200.normal.textColor=new Color (222/255f, 174/255f, 140/255f);
		Button200.hover.textColor=new Color (222/255f, 174/255f, 140/255f);
		Button200.active.textColor=new Color (222/255f, 174/255f, 140/255f);

		Button200off = new GUIStyle ();
		Button200off.normal.background=Resources.Load ("GUI/MainMenu/Button200c") as Texture2D;
		Button200off.font = Resources.Load ("GUI/Fonts/BigFont.tff") as Font;
		Button200off.alignment = TextAnchor.MiddleCenter;		
		Button200off.normal.textColor=new Color (222/255f, 174/255f, 140/255f);


		InGameUIBG = new GUIStyle ();	
		InGameUIBG.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameUIBG.normal.background.filterMode = FilterMode.Point;
		InGameUIBG.alignment = TextAnchor.MiddleCenter;		
		InGameUIBO = new GUIStyle ();	
		InGameUIBO.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameUIBO.normal.background.filterMode = FilterMode.Point;
		InGameUIBO.alignment = TextAnchor.MiddleCenter;		
		InGameUIBU = new GUIStyle ();	
		InGameUIBU.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameUIBU.normal.background.filterMode = FilterMode.Point;
		InGameUIBU.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		InGameUIBU.normal.textColor = Color.black;
		InGameUIBU.fontSize = 12;
		InGameUIBUoff = new GUIStyle ();	
		InGameUIBUoff.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameUIBUoff.normal.background.filterMode = FilterMode.Point;
		InGameUIBUoff.alignment = TextAnchor.MiddleCenter;		
		InGameUILA = new GUIStyle ();	
		InGameUILA.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameUILA.normal.background.filterMode = FilterMode.Point;
		InGameUILA.alignment = TextAnchor.MiddleCenter;		
		InGameUILA.normal.textColor = Color.black;
		InGameUILA.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		InGameUILA.fontSize = 10;
		InGameUISL = new GUIStyle ();	
		InGameUISL.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameUISL.normal.background.filterMode = FilterMode.Point;
		InGameUISL.alignment = TextAnchor.MiddleCenter;		
		InGameMENU = new GUIStyle ();	
		InGameMENU.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameMENU.normal.background.filterMode = FilterMode.Point;
		InGameMENU.alignment = TextAnchor.MiddleCenter;		
		InGameMENU.normal.textColor = Color.black;
		InGameMENU.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		InGameMEBU = new GUIStyle ();	
		InGameMEBU.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameMEBU.normal.background.filterMode = FilterMode.Point;
		InGameMEBU.alignment = TextAnchor.MiddleCenter;		
		InGameMEBU.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		InGameMEBU.normal.textColor = Color.black;
		InGameMIMA = new GUIStyle ();	
		InGameMIMA.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameMIMA.normal.background.filterMode = FilterMode.Point;
		InGameMIMA.alignment = TextAnchor.MiddleCenter;

		InGameUIAR = new GUIStyle ();
		InGameUIAR.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameUIAR.normal.background.filterMode = FilterMode.Point;		
		InGameUIAB = new GUIStyle ();
		InGameUIAB.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameUIAB.normal.background.filterMode = FilterMode.Point;
		InGameUIAS = new GUIStyle ();
		InGameUIAS.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameUIAS.normal.background.filterMode = FilterMode.Point;

		InGameREBA = new GUIStyle ();
		InGameREBA.normal.background = Resources.Load ("GUI/MainMenuUI") as Texture2D;
		InGameREBA.normal.background.filterMode = FilterMode.Point;
		InGameREBA.normal.textColor = Color.black;
		InGameREBA.font = Resources.GetBuiltinResource<Font>("Arial.ttf");*/
	}

	public void Rescale(float Multiplier){

		MainMenuBU.fontSize = Mathf.RoundToInt (30f);
		/*SkullScrollThumb.padding = new RectOffset (Mathf.RoundToInt(16), Mathf.RoundToInt(16), Mathf.RoundToInt(16), Mathf.RoundToInt(16));
		SearchListIndex.fontSize = Mathf.RoundToInt(13);
		SearchListIndexSelected.fontSize = Mathf.RoundToInt(13);
		MainMenuButton.fontSize = Mathf.RoundToInt(36);
		MainMenuLabel.fontSize = Mathf.RoundToInt(13);
		EditorLabel.overflow = new RectOffset((int)(10),(int)(10),(int)(2),(int)(2));
		EditorLabel.fontSize = Mathf.RoundToInt(13);
		StoneOverlay.fontSize = Mathf.RoundToInt(13);
		MainMenuTitle.fontSize = Mathf.RoundToInt(13);
		HeroName.fontSize = Mathf.RoundToInt(17);
		Droplist153Closed.overflow = new RectOffset((int)(12),(int)(10),(int)(2),(int)(2));
		Droplist153Closed.fontSize = Mathf.RoundToInt(13);
		Droplist153.overflow = new RectOffset((int)(12),(int)(10),(int)(2),(int)(2));
		Droplist153.fontSize = Mathf.RoundToInt(13);
		Button200off.overflow = new RectOffset((int)(25),(int)(25),(int)(5),(int)(5));
		Button200off.fontSize = Mathf.RoundToInt(14);
		Button200.overflow = new RectOffset((int)(25),(int)(25),(int)(5),(int)(5));
		Button200.fontSize = Mathf.RoundToInt(14);*/
	}
}