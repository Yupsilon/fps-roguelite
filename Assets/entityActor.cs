﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityActor:entityBase{

	public static int Animation_Fire=MaxSides;
	public static int Animation_Death=MaxSides+1;

	public List<propertyModifier> Modifiers = new List<propertyModifier>();
	public List<int> ModifierStates= new List<int>();
	public List<float> ModifierProperties= new List<float>();

	public Vector3 lastPuddlePoint=Vector2.zero;

	public float Life=100;
	public float nDeaths=0;

	public override void onCreated ()
	{
		base.onCreated ();
		UpdateStates ();
	}

	public override Vector2 GetFlight()
	{
		Vector2 Return = Vector2.zero;
		if (ModifierStates.Contains(propertyModifier.modstate_Flying) ){
			Return.x=-1;
		}
		if (ModifierStates.Contains(propertyModifier.modstate_Ghost)) {
			Return.y=1;
		}
		return Return;
	}

	public void SetAcceleration(float x, float y)
	{
		Acceleration = new Vector2 (x,y) * ModifierProperties [propertyModifier.property_speed];

	}

	public override float GetMass ()
	{
		return ModifierProperties [propertyModifier.property_mass];
	}

	public void AddModifier(StateInGame game, propertyModifier oData, int Type){

		oData.Duration = game.gameTime+oData.totDuration;

		if (oData.totDuration < 0) {
			oData.Duration = game.gameTime;}
		switch (Type) {
		case 0: //Multiple 
			Modifiers.Add (oData);
			break;
		case 1: //Replace 
			RemoveModifierByName(oData.ModifierName);
			Modifiers.Add (oData);
			break;
		case 2: //Unique 
			if (!HasModifier (oData.ModifierName)) {
				Modifiers.Add (oData);
			}
			break;
		}
		UpdateStates ();}
	public void RemoveModifierByName(string Name){

		List<propertyModifier> Temp = new List<propertyModifier>();
		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.ModifierName == Name) {
				Temp.Add (Mod);
			}
		}
		foreach (propertyModifier Mod in Temp) {
			Mod.Die (true);
			Modifiers.Remove (Mod);
		}
		UpdateStates ();
	}
	public bool HasModifier(string Name){
		return GetModifierByName (Name) != null;
	}

	public propertyModifier GetModifierByName(string Name){
		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.ModifierName == Name) {return Mod;
			}
		}
		return null;
	}

	public void UpdateStates()
	{
		ModifierStates.Clear();
		ModifierProperties.Clear ();
		for (int i = 0; i < propertyModifier.property_max; i++) {
			ModifierProperties.Add (1f);
		}

		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.ModifierStates!=null){foreach (int i in Mod.ModifierStates) {
					if (!ModifierStates.Contains (i)) {
						ModifierStates.Add (i);
					}
				}
			}
			if (Mod.ModifierProperties!=null){
				foreach (float[] d in Mod.ModifierProperties) {

					ModifierProperties  [(int)d [0]] *= d [1];

				}
			}
		}
	}

	public virtual float getPuddleRadius()
	{
		return 0;
	}

	public override void Update ()
	{
		if (HasModifier ("Animate")) {
			propertyModifier Mod = GetModifierByName ("Animate");

			float Modifier = Mathf.Abs(Mod.GetModifierDuration (game) / Mod.totDuration);
			fFrame = Mathf.Clamp((MaxFrames ) * Modifier,0,(MaxFrames - 1));
		} else {
			fFrame = 0;
		}

		if (getPuddleRadius()>0 && (lastPuddlePoint - Position).magnitude > getPuddleRadius()) {
			game.generatePuddle (Position, getPuddleRadius()*DrawTheWorld.DefaultTileSize/2f,5, squirtColor, 0);
			lastPuddlePoint =Position;
		}

		base.Update ();

		List<propertyModifier> Temp = new List<propertyModifier>();
		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.totDuration>0 && Mod.Duration <= game.gameTime) {
				Temp.Add (Mod);
			}
		}
		foreach (propertyModifier Mod in Temp) {

		Mod.Die(true);
			Modifiers.Remove (Mod);
		}
		if (Temp.Count > 0) {

			UpdateStates ();}
	}
	public override bool CanAct(){

		return !ModifierStates.Contains(propertyModifier.modstate_Stunned) && base.CanAct ();

	}
	public bool CanAttack(){
		return !ModifierStates.Contains (propertyModifier.modstate_Disarmed);}


	public override bool TakeProjectiles()
	{return !isDead && !ModifierStates.Contains(propertyModifier.modstate_Phased);}


	public override float GetElasticity ()
	{
		return ModifierProperties[propertyModifier.property_elasticity];
	}

	public override float GetSpeed()
	{
		return game.GetSpeed();
	}
	public override bool DoesCollide ()
	{
		return !ModifierStates.Contains(propertyModifier.modstate_Phased);
	}

	public override float GetFriction ()
	{
		return base.GetFriction ()*ModifierProperties[propertyModifier.property_friction];
	}

	public void TakeDamage(float Damage)
	{
		if (ModifierStates[propertyModifier.modstate_Invulnerable]==1){
			Life -= Damage*ModifierProperties[propertyModifier.property_incomingdamage];

		if (Life <= 0) {
			Die ();
			}}
	}

	public virtual int GetFacing()
	{
		if (HasModifier ("dead")) {
			return Animation_Death;

		} else {

			float rAngle = (Rotation+45 ) % 360;

			if (rAngle < 0) {
				rAngle = 360 + rAngle;
			}

			int final = Mathf.FloorToInt ((rAngle) / 360f * MaxSides);

			if (final== 2 && HasModifier ("reload")) {
				return Animation_Fire;

			}
			return final;
		}
	}

	public override void Draw ()
	{
		base.Draw ();

		int iSprite = GetFacing () * MaxFrames + Mathf.FloorToInt (fFrame);
		gameObject.GetComponent<SpriteRenderer> ().sprite =SpriteData[iSprite] ;
	}

	public override void DrawSprites(string nSprite)
	{
		SpriteData = new List<UnityEngine.Sprite> ();
		Texture2D BaseTexture = DrawTheWorld.LoadTex (nSprite);
		float SpriteSize = (BaseTexture.width/MaxFacings);
		MaxFrames = Mathf.CeilToInt(BaseTexture.height/SpriteSize);	
		BaseTexture.filterMode = FilterMode.Point;

		for (int iX = 0; iX < MaxFacings; iX++) {
			for (int iY = MaxFrames-1; iY >=0 ; iY--) {

				Rect spriteRect = new Rect (iX * SpriteSize, iY * SpriteSize, SpriteSize, Mathf.Min (BaseTexture.height, SpriteSize));

				SpriteData.Add (Sprite.Create (BaseTexture,spriteRect,
					new Vector2(0.5f,0.25f)));
			}
		}
	}

	public float GetMaxLife()
	{
		return 100;
	}

	public float TakeDamage(float d)
	{
		Life -= d;

		if (Life < 0) {
			nDeaths++;
			Life = GetMaxLife ();
			AddModifier(game, new propertyModifer("dead",
		}
	}

	public void Knockback(Vector2 dir, float dur)
	{

	}
}