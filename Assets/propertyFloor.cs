﻿using System;
using UnityEngine;
using System.Collections.Generic;
	public class propertyFloor
	{
	StateInGame game;
	List<PixelData> changes;
	public Texture2D floorTexture; 
	public int[,] TileSetData;
	public static float FloorMulti = 0.6f;

	int playerTiles=0;
	int enemyTiles=0;

	public propertyFloor (StateInGame g, int size)
		{
		game = g;
		floorTexture = new Texture2D((int)((size)*DrawTheWorld.DefaultTileSize*FloorMulti),(int)((size)*DrawTheWorld.DefaultTileSize*FloorMulti));
		TileSetData = new int[floorTexture.width, floorTexture.height];

		for (int iX = 0; iX < floorTexture.width; iX++) {

			for (int iY=0; iY<floorTexture.height; iY++){

				floorTexture.SetPixel (iX, iY, Color.clear);
				TileSetData [iX, iY] = 0;

			}
		}
		floorTexture.Apply ();
		floorTexture.filterMode = FilterMode.Point;

		changes = new List<PixelData> ();
	}

	public void RemoveVector(Vector2 center)
	{
		changes.RemoveAll (delegate(PixelData obj) {
			return 	obj.point == center;
		});
	}

	public void generatePuddle(int x, int y, int r,Color32 col, int playerID)
	{
		for (int ix = x - r; ix <= x + r; ix++) {
			for (int iy = y - r; iy <= y + r; iy++) {
				if (ix >= 0 && ix < floorTexture.width &&
				    iy >= 0 && iy < floorTexture.height && 
					(new Vector2 (ix - x, iy - y).magnitude)	 <= r) {
					//RemoveVector (new Vector2 (ix, iy));
					changes.Add (new PixelData (new Vector2Int (ix, iy),col,playerID));
				}
			}
		}
	}

	public void Update()
		{
		foreach (PixelData Zim in changes){

			int ix = Zim.point.x;
			int iy = Zim.point.y;
			if (Zim.color.a < 1) {
				if (floorTexture.GetPixel (ix, iy).a == 0) {
					floorTexture.SetPixel ((int)ix, (int)iy, Zim.color);	
				} else {
					//TBD
				}
			} else {
				floorTexture.SetPixel ((int)ix, (int)iy, Zim.color);
			}

			if (Zim.player == 1) {//enemy paints
				if (TileSetData [ix, iy] == 2) {
					playerTiles--;
				}
				TileSetData[ix,iy]=1;
				enemyTiles++;
			} else if (Zim.player == 2) {//player paints
				if (floorTexture.GetPixel (ix, iy) == Zim.color) {
					if (TileSetData [ix, iy] == 1) {
						enemyTiles--;
					}
					TileSetData[ix,iy]=2;
					playerTiles++;
				}
			}
			}
		floorTexture.Apply ();
			changes.Clear();
	}

	public int GetTilesCount()
	{
		return floorTexture.width*floorTexture.height;
	}

	public float GetConvertPercentage()
	{
		return playerTiles / GetTilesCount ();
	}
}

public class PixelData
{
	public Vector2Int point;
	public Color32 color;
	public int player;

	public PixelData (Vector2Int p, Color32 c, int i)
	{
		point = p;
		color = c;
		player = i;
	}
}
