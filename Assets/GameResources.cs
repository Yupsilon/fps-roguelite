﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class GameResources:MonoBehaviour
{public List<ArrayList> Memory=new List<ArrayList>{new ArrayList(), new ArrayList()};
	public List<GameObject> GUIData=new List<GameObject>();
	public InGameGUI gui;
	public StateInGame activeGame;

	public void OnEnable()
	{
		gui = new InGameGUI (this);
		activeGame = new StateInGame (this);
	}

	public Texture2D LoadTexture(string Folder, string FileName)
	{

		return LoadTexture(Folder,FileName,false);
	}

	public Texture2D LoadTexture(string Folder, string FileName,bool readable)
	{
		foreach (string ext in GameDirectory.ImageFileExtension) {
			if (LoadTexture(Folder,FileName,ext)
				!=null)
			{return LoadTexture(Folder,FileName,ext,readable);}
			//Debug.Log ("No file found at location "+GameDirectory.DirectoryFolder + Folder+FileName+ext);
		}
		return null;
	}

	public Texture2D LoadTexture(string Folder, string FileName,string Exension)
	{
		return LoadTexture (Folder, FileName, Exension,false);
	}

	public Texture2D LoadTexture(string Folder, string FileName,string Exension,bool readable)
	{
		Texture2D myTexture = null;

		if (GetIndex <Texture2D> (Folder + FileName) != null) {

			myTexture = GetIndex <Texture2D> (Folder + FileName);
		} else {
			if (UnityEditor.AssetDatabase.LoadAssetAtPath<Texture2D> (GameDirectory.AssetFolder + Folder + FileName + Exension) != null) {
				myTexture = UnityEditor.AssetDatabase.LoadAssetAtPath<Texture2D> (GameDirectory.AssetFolder + Folder + FileName + Exension);
				if (readable) {
					UnityEditor.TextureImporter ti = UnityEditor.TextureImporter.GetAtPath (GameDirectory.AssetFolder + Folder + FileName + Exension) as UnityEditor.TextureImporter;

					ti.isReadable = true;

					UnityEditor.AssetDatabase.ImportAsset(GameDirectory.AssetFolder + Folder + FileName + Exension);
					UnityEditor.AssetDatabase.Refresh();
				}

			} else if (File.Exists (GameDirectory.DirectoryFolder+GameDirectory.DataFolder + Folder + FileName + Exension)) {

				string url = "file:///" + GameDirectory.DirectoryFolder+GameDirectory.DataFolder + Folder + FileName + Exension;
				myTexture = new Texture2D (32, 32);

				WWW www = new WWW (url);		

				while (www.isDone == false) {
					new WaitForFixedUpdate ();

				}

				if (www.isDone) {
					www.LoadImageIntoTexture (myTexture);
				}
			} else {
				//				Debug.Log ("No file found in "+Folder + FileName+Exension);
			} 


			if (myTexture != null) {
				AddIndex (Folder + FileName, myTexture);
				myTexture.name = FileName;
				myTexture.filterMode = FilterMode.Point;
				myTexture.anisoLevel = 10;
			}
		}


		return myTexture;	
	}
	public ltype Load<ltype>( string Name, string folder, string ext)
	{
		string Dir = folder + Name + ext;
		//Debug.Log(GameDirectory.DirectoryFolder + Dir);
		if (GetIndex<ltype> (typeof(ltype).ToString()+Name) != null) {

			return GetIndex<ltype>( typeof(ltype)+Name);
		}
		else if (UnityEditor.AssetDatabase.LoadAssetAtPath<TextAsset>(GameDirectory.AssetFolder+folder + Name + ".xml")!=null)
		{
			TextAsset LoadData = UnityEditor.AssetDatabase.LoadAssetAtPath<TextAsset> (GameDirectory.AssetFolder + folder + Name + ".xml");

			ltype TempData = (ltype)(new XmlSerializer (typeof(ltype)).Deserialize (new StringReader (LoadData.text))) ;			
			if (TempData!=null)
			{
				AddIndex( Dir,TempData);
			}		
			return TempData;
		}
		else if (File.Exists (GameDirectory.DirectoryFolder+GameDirectory.DataFolder+Dir) == true) {
			FileStream SaveSpace = new FileStream (GameDirectory.DirectoryFolder+GameDirectory.DataFolder+Dir, FileMode.Open);

			ltype TempData = (ltype)(new XmlSerializer (typeof(ltype)).Deserialize (SaveSpace));			

			SaveSpace.Close ();

			if (TempData != null) {
				AddIndex ( typeof(ltype).ToString()+Name, TempData);
			} 

			return TempData;
		} else {
			//Debug.Log ("No file found in "+Dir);

			return default(ltype);
		}
	}
	public void AddIndex(string NAME, object OBJ)
	{
		Memory[0].Add(OBJ);
		Memory[1].Add(NAME);
	}
	public type GetIndex<type>( string NAME)	{
		if (Memory[1].IndexOf (NAME) >= 0) {
			return (type)Memory[0] [Memory[1].IndexOf (NAME)];
		} else {
			return default(type);
		}
	}
}

