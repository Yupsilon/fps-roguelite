﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class NewMenus
{
	public static Vector2 ScreenResolution = new Vector2 (1600f, 900f);

	public static void ClearGUI(InGameGUI game)
	{
		for (int i=0; i<game.GUIData.Count; i++) {
			GameObject.Destroy(game.GUIData [i]);
			game.GUIData.RemoveAt (i);
			i--;
		}
	}

	public static GameObject RectTransform(InGameGUI game, Rect DrawRect)
	{
		GameObject newText = new GameObject();

		RectTransform Pos = newText.AddComponent<RectTransform> ();
		Pos.SetParent(game.GUICanvas.gameObject.transform);
		Pos.pivot = new Vector2 (0.5f, 0.5f);
		RectToTransform (Pos,DrawRect,false);
		//Pos.localScale = new Vector2 (DrawRect.width/100, DrawRect.height/100);

		return newText;
	}

	public static Vector2 CameraPointToReal(Vector2 Point)
	{
		return new Vector2(
			(Point.x)/Screen.width*NewMenus.ScreenResolution.x,
			(Point.y)/Screen.height*NewMenus.ScreenResolution.y
		);
	}

	public static Vector2 RealPointToCamera(Vector2 Point)
	{
		return new Vector2(
			(Point.x)/Screen.width*NewMenus.ScreenResolution.x,
			(Screen.height-Point.y)/Screen.height*NewMenus.ScreenResolution.y
		);
	}

	public static Rect FitRectOnScreen(Rect Ref)
	{
		return new Rect(Mathf.Max (0, Mathf.Min(ScreenResolution.x-Ref.width,Ref.xMin)),Mathf.Max (0, Mathf.Min(ScreenResolution.y-Ref.height,Ref.yMin)),Ref.width,Ref.height);
	}

	public static void RectToTransform(RectTransform Manipulator, Rect DrawRect, bool ScaleChildren)
	{
		Vector2 Mult = new Vector2(Screen.width / ScreenResolution.x,Screen.height / ScreenResolution.y);

		Rect NewRect = new Rect (
			(DrawRect.x + DrawRect.width / 2f - ScreenResolution.x/2) * Mult.x,
			0 - (DrawRect.y + DrawRect.height / 2f - ScreenResolution.y/2)*Mult.y,
			DrawRect.width * Mult.x, DrawRect.height *Mult.y);

		if (ScaleChildren && Manipulator.childCount > 0) {

			float mW=NewRect.width/Manipulator.sizeDelta.x;
			float mH=NewRect.height/Manipulator.sizeDelta.y;

			for (int I=0; I<Manipulator.childCount; I++)
			{
				RectTransform Child = Manipulator.GetChild(I) as RectTransform;

				Child.sizeDelta = new Vector2 (Child.sizeDelta.x*mW,Child.sizeDelta.y*mH);
				Child.anchoredPosition = new Vector2(Child.anchoredPosition.x*mW,Child.anchoredPosition.y*mH);
			}
		}

		Manipulator.sizeDelta = new Vector2 (NewRect.width,NewRect.height);
		Manipulator.anchoredPosition = new Vector2(NewRect.x,NewRect.y);
	}

	public static GameObject GUIWindow (InGameGUI game,GUIStyle Style, Rect DrawRect)
	{
		if (Style == null) {
			return null;
		}

		if (Style.normal.background != null &&
			Style.border.bottom + Style.overflow.bottom != 0 &&
			Style.border.top + Style.overflow.top != 0 &&
			Style.border.right + Style.overflow.right != 0 &&
			Style.border.left + Style.overflow.left != 0) {

			List<GameObject> All = new List<GameObject> ();

			GameObject myStuff = null;

			for (int iX = 0; iX < 3; iX++) {
				for (int iY = 0; iY < 3; iY++) {

					Rect SpriteRect = new Rect(0,0,0,0);
					Rect RealRect = new Rect(DrawRect.xMin,DrawRect.yMin,DrawRect.width,DrawRect.height);

					//Pos.localScale = new Vector2 (DrawRect.width * Multiplier / Style.normal.background.width, DrawRect.height * Multiplier / Style.normal.background.height);
					//Pos.position = new Vector3 (Pos.parent.transform.position.x + DrawRect.x * Multiplier / Style.normal.background.width, Pos.parent.transform.position.y + DrawRect.y * Multiplier / Style.normal.background.height, -1);

					if (iX == 0) {
						SpriteRect.xMin = 0;
						SpriteRect.xMax = Style.overflow.left;

						RealRect.xMin = DrawRect.xMin-Style.overflow.left;
						RealRect.width = Style.overflow.left;
					} else if (iX == 1) {
						SpriteRect.xMin = Style.overflow.left;
						SpriteRect.xMax = Style.normal.background.width-Style.overflow.right-Style.overflow.left;

						RealRect.xMin = DrawRect.xMin;
						RealRect.width = DrawRect.width ;
					} else {
						SpriteRect.xMin = Style.normal.background.width-Style.overflow.right;
						SpriteRect.xMax = Style.overflow.right;

						RealRect.xMin = DrawRect.xMax;
						RealRect.width =Style.overflow.right;
					}

					if (iY == 0) {
						SpriteRect.yMin = Style.normal.background.height-Style.overflow.bottom;
						SpriteRect.yMax = Style.overflow.bottom;

						RealRect.yMin = DrawRect.yMin - Style.overflow.top;
						RealRect.yMax = DrawRect.yMin;
					} else if (iY == 1) {
						SpriteRect.yMin = Style.overflow.top;
						SpriteRect.yMax = Style.normal.background.height-Style.overflow.bottom-Style.overflow.top;

						RealRect.yMin = DrawRect.yMin;
						RealRect.yMax = DrawRect.yMax ;
					} else {
						SpriteRect.yMin = 0;
						SpriteRect.yMax = Style.overflow.top;

						RealRect.yMin = DrawRect.yMax;
						RealRect.yMax = DrawRect.yMax + Style.overflow.bottom;
					}

					if (RealRect.width != 0 && RealRect.height != 0 && SpriteRect.width != 0 && SpriteRect.height != 0) 
					{

						GameObject newGameObject = RectTransform (game, RealRect);
						newGameObject.name = "Window";

						Image Draw = newGameObject.AddComponent<Image> ();
						Draw.sprite = Sprite.Create (Style.normal.background, new Rect (SpriteRect.xMin, SpriteRect.yMin, SpriteRect.xMax, SpriteRect.yMax), new Vector2 (0.5f, 0.5f));
						//						Draw.material = Resources.GetBuiltinResource<Material> ("Sprites-Default.mat");
						Draw.raycastTarget = false;

						game.GUIData.Add (newGameObject);

						if (iX == 1 && iY == 1) {
							myStuff = newGameObject;
						} else {
							All.Add (newGameObject);
						}
					}
				}
			}

			foreach (GameObject myObj in All) {
				myObj.transform.SetParent ( myStuff.transform);
			}

			return myStuff;
		} else {

			GameObject newGameObject = RectTransform(game, DrawRect);
			newGameObject.name = "Window";


			Image Draw = newGameObject.AddComponent<Image> ();
			if (Style.normal.background != null) {
				Draw.sprite = Sprite.Create (Style.normal.background, new Rect (0, 0, Style.normal.background.width, Style.normal.background.height), new Vector2 (0.5f, 0.5f));
				//				Draw.material = Resources.GetBuiltinResource<Material> ("Sprites-Default.mat");
			} else {
				Draw.color=new Color(0,0,0,0);
			}


			game.GUIData.Add (newGameObject);

			return newGameObject;
		}
	}

	public static GameObject GUISprite (InGameGUI game, Sprite Content, GUIStyle Style, Rect DrawRect)
	{
		GameObject newImage = RectTransform (game, DrawRect);

		if (Style != null) {
			newImage.transform.SetParent (GUIWindow (game, Style, DrawRect).transform);
		}

		newImage.name = "Content Image";

		Image Data = newImage.AddComponent<Image> ();
		Data.sprite = Content;
		//Data.material = Resources.GetBuiltinResource<Material> ("Sprites-Default.mat");
		Data.raycastTarget = false;

		game.GUIData.Add (newImage);

		return newImage;
	} 

	public static GameObject GUIInputField (InGameGUI game, string  Content, GUIStyle Style, Rect DrawRect)
	{
		GameObject Window = GUIWindow(game,Style,DrawRect);

		GameObject gText = GUIContent (game, new UnityEngine.GUIContent (""), Style, DrawRect);
		GameObject gPlaceholder = GUIContent (game, new UnityEngine.GUIContent (Content), Style, DrawRect);

		Window.AddComponent<InputField> ();
		InputField iField = Window.GetComponent<InputField> ();
		iField.text = Content;

		iField.transition = Selectable.Transition.ColorTint;
		iField.colors = ColorBlock.defaultColorBlock;
		iField.textComponent = gText.GetComponent<Text>();
		iField.placeholder = gPlaceholder.GetComponent<Text> ();

		gPlaceholder.name = "Placeholder";

		gText.transform.SetParent (Window.transform);
		gPlaceholder.transform.SetParent (Window.transform);

		return Window;
	}

	public static GameObject GUIContent (InGameGUI game, GUIContent Content, GUIStyle Style, Rect DrawRect)
	{
		if (Content == null) {
			return null;
		}

		if (Content.image != null) {

			GameObject newImage = RectTransform (game, DrawRect);
			newImage.name = "Content Image";

			Image Data = newImage.AddComponent<Image> ();
			Data.sprite = Sprite.Create (Content.image as Texture2D, new Rect (0, 0, Content.image.width, Content.image.height), new Vector2 (0.5f, 0.5f));
			//			Data.material = Resources.GetBuiltinResource<Material> ("Sprites-Default.mat");
			Data.raycastTarget = false;

			game.GUIData.Add (newImage);

			return newImage;
		} 

		if (Content.text!=null)
		{
			GameObject newText = RectTransform (game, DrawRect);
			newText.transform.localScale = new Vector3 (1, 1, 1);
			newText.name = "Content Text";

			Text Data = newText.AddComponent<Text> ();
			Data.text = Content.text;
			Data.raycastTarget = false;

			Data.resizeTextForBestFit = true;
			Data.resizeTextMinSize = 0;
			Data.resizeTextMaxSize = 10;

			if (Style != null) {
				Data.font = Style.font;
				Data.fontStyle = Style.fontStyle;
				Data.fontSize = Style.fontSize*Math.Min(1600/Screen.width,900/Screen.height);
				Data.resizeTextMaxSize = Data.fontSize;
				Data.alignment = Style.alignment;
				Data.color = Style.normal.textColor;
				Data.verticalOverflow =  VerticalWrapMode.Overflow;
				Data.horizontalOverflow = HorizontalWrapMode.Wrap;
			} else {
				Data.color = Color.black;
				Data.font =  Resources.GetBuiltinResource<Font>("Arial.ttf");
			}

			game.GUIData.Add (newText);

			return newText;
		}

		return null;
	}


	public static GameObject GUIContentBox (InGameGUI game, GUIContent Content, GUIStyle Style, Rect DrawRect,float Borders)
	{
		Borders = 0f;
		GameObject Window = GUIWindow(game,Style,DrawRect);
		GameObject goContent = GUIContent (game, Content, Style, new Rect(DrawRect.xMin+Borders,DrawRect.yMin+Borders,DrawRect.width-Borders*2,DrawRect.height-Borders*2));

		if (Window != null) {
			goContent.transform.SetParent( Window.transform);
		}
		return goContent;
	}

	public static GameObject GUITextBox (InGameGUI game, string tText, GUIStyle Style, Rect DrawRect,float Borders)
	{
		GameObject goContent = GUIContentBox (game, new UnityEngine.GUIContent (tText), Style, DrawRect,Borders);

		goContent.GetComponent<Text> ().verticalOverflow = VerticalWrapMode.Truncate;
		goContent.GetComponent<Text> ().horizontalOverflow = HorizontalWrapMode.Wrap;

		/*goContent.AddComponent<ContentSizeFitter> ();
		goContent.GetComponent<ContentSizeFitter> ().horizontalFit = ContentSizeFitter.FitMode.Unconstrained;
		goContent.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.PreferredSize;*/

		return goContent.transform.parent.gameObject;
	}

	public static GameObject GUIButton (InGameGUI game,GUIContent Content, GUIStyle Style, Rect DrawRect, GUIButtonData.ButtonAction ClickFunction, GUIStyle disabledTextureStyle)
	{
		GameObject newGameObject = GUIWindow(game,Style, DrawRect);
		if (newGameObject != null && Content!=null) {
			GameObject gContent = GUIContent (game, Content, Style, DrawRect);
			gContent.transform.SetParent (newGameObject.transform);
		}
		newGameObject.name = "Button";
		newGameObject.transform.position = new Vector3 (newGameObject.transform.position.x, newGameObject.transform.position.y, 10);
		newGameObject.GetComponent<Image> ().raycastTarget = true;

		Button Temp = newGameObject.AddComponent<Button> ();
		Temp.transition = Selectable.Transition.SpriteSwap;

		GUIButtonData btnData = newGameObject.AddComponent<GUIButtonData> ();
		newGameObject.AddComponent<EventTrigger> ();

		btnData.actualTexture = Style.normal.background;
		btnData.Enabled = Style.normal.background;
		btnData.onHover = Style.hover.background;
		btnData.onClick = Style.active.background;
		if (disabledTextureStyle != null) {
			btnData.Disabled = disabledTextureStyle.normal.background;
		}
		btnData.AddMouseOverAction (ClickFunction);

		btnData.AddMouseOverAction (delegate {
			btnData.MouseOver=true;
		},
			delegate {
				btnData.MouseOver=false;	
			});


		game.GUIData.Add (newGameObject);

		return Temp.gameObject;
	}

	public static GameObject GUISpriteButton (InGameGUI game,Sprite Content, GUIStyle Style, Rect DrawRect, GUIButtonData.ButtonAction ClickFunction, Texture2D disabledTexture)
	{
		GameObject newGameObject = GUISprite(game,Content,Style, DrawRect);

		newGameObject.name = "Sprite Button";
		newGameObject.transform.position = new Vector3 (newGameObject.transform.position.x, newGameObject.transform.position.y, 10);
		newGameObject.GetComponent<Image> ().raycastTarget = true;

		Button Temp = newGameObject.AddComponent<Button> ();
		Temp.transition = Selectable.Transition.SpriteSwap;

		newGameObject.AddComponent<EventTrigger> ();
		GUIButtonData btnData = newGameObject.AddComponent<GUIButtonData> ();

		btnData.actualTexture = Style.normal.background;
		btnData.Enabled = Style.normal.background;
		btnData.onHover = Style.hover.background;
		btnData.onClick = Style.normal.background;
		btnData.Disabled = disabledTexture;

		btnData.AddMouseOverAction(ClickFunction);		

		game.GUIData.Add (newGameObject);

		return Temp.gameObject;
	}

	public delegate void toggleFunc(bool bfu); 
	public static GameObject GUIToggleButton (InGameGUI game, 
		Rect DrawRect, bool Variable,toggleFunc ClickFunction, 
		GUIStyle toggleOn, GUIStyle toggleOff, GUIStyle disabledTextureStyle)
	{
		GUIStyle Style = toggleOff;
		if (Variable) {
			Style = toggleOn;
		}
		GameObject newGameObject = GUIWindow(game,Style, DrawRect);
		newGameObject.name = "Content Image";
		newGameObject.transform.SetParent(newGameObject.transform);

		newGameObject.transform.position = new Vector3 (newGameObject.transform.position.x, newGameObject.transform.position.y, 10);
		newGameObject.GetComponent<Image> ().raycastTarget = true;

		Button Temp = newGameObject.AddComponent<Button> ();
		Temp.transition = Selectable.Transition.SpriteSwap;

		GUIButtonData btnData = newGameObject.AddComponent<GUIButtonData> ();
		newGameObject.AddComponent<EventTrigger> ();


		btnData.actualTexture = Style.normal.background;
		btnData.Enabled = Style.normal.background;
		btnData.onHover = Style.hover.background;
		btnData.onClick = Style.normal.background;
		if (disabledTextureStyle != null) {
			btnData.Disabled = disabledTextureStyle.normal.background;
		}

		btnData.AddMouseOverAction (delegate {
			btnData.MouseOver=true;
		},
			delegate {
				btnData.MouseOver=false;	
			});
		bool boolData = Variable;
		if (ClickFunction != null) {
			Temp.onClick.AddListener ((delegate {
				boolData=!boolData;
				ClickFunction (boolData);
				if (boolData){


					btnData.actualTexture=toggleOn.normal.background;
				}
				else {

					btnData.actualTexture=toggleOff.normal.background;
				}
				Sprite tempSprite =newGameObject.GetComponent<Image> ().sprite;

				newGameObject.GetComponent<Image> ().sprite = Sprite.Create (btnData.actualTexture,
					new Rect (tempSprite.rect.xMin, tempSprite.rect.yMin,
						Mathf.Min (tempSprite.rect.width, (btnData.actualTexture.width - tempSprite.rect.xMin)),
						Mathf.Min (tempSprite.rect.height, (btnData.actualTexture.height - tempSprite.rect.yMin)))
					, tempSprite.pivot);
			}));
		}

		game.GUIData.Add (newGameObject);

		return Temp.gameObject;
	}
}

public class GUIButtonData:MonoBehaviour,IPointerClickHandler//,IPointerEnterHandler,IPointerExitHandler
{
	public InGameGUI game;
	public Texture2D Enabled;
	public Texture2D onHover;
	public Texture2D onClick;
	public Texture2D Disabled;
	public Texture2D actualTexture;
	public bool MouseOver=false;
	float lastClickTime=0f;

	public delegate void ButtonAction(PointerEventData eventData);
	ButtonAction clickFunction = delegate(PointerEventData eventData) {};

	public void AddMouseOverAction(ButtonAction act)
	{
		clickFunction = act;
	}

	public void Update()
	{
		if (this.gameObject.GetComponent<Button> ().interactable) {
			if (MouseOver) {
				ChangeTexture (onHover);
			} else if (lastClickTime - Time.time < 1f / 3f) {

				ChangeTexture (onClick);
			}else {
				ChangeTexture (Enabled);
			}
		} else {
			ChangeTexture (Disabled);
		}
	}
	public void ChangeTexture(Texture2D Texture)
	{
		if (Texture == null) {
			actualTexture=Enabled;
		} else {
			if (actualTexture.name == Texture.name) {
				return;
			}
			actualTexture=Texture;
		}


		if (actualTexture != null) {
			for (int iChild = 0; iChild < gameObject.transform.childCount; iChild++) {
				GameObject gChild = gameObject.transform.GetChild (iChild).gameObject;
				if (gChild.GetComponent<Image> () != null && gChild.name == "Content Image") {
					Sprite tempSprite = gChild.GetComponent<Image> ().sprite;

					gChild.GetComponent<Image> ().sprite = Sprite.Create (actualTexture,
						new Rect (tempSprite.rect.xMin, tempSprite.rect.yMin,
							Mathf.Min (tempSprite.rect.width, (actualTexture.width - tempSprite.rect.xMin)),
							Mathf.Min (tempSprite.rect.height, (actualTexture.height - tempSprite.rect.yMin)))
						, tempSprite.pivot);
				}
			}
		}
	}

	public void ChangeText(string Value)
	{
		if (gameObject.GetComponent<Text> () == null) {
			gameObject.transform.Find ("Content Text").gameObject.GetComponent<Text> ().text = Value;
		} else {
			gameObject.GetComponent<Text> ().text = Value;
		}
	}

	public delegate void EventAction(BaseEventData data);
	public void AddMouseOverAction(EventAction OnEnter, EventAction onExit)
	{
		EventTrigger trigger = gameObject.GetComponent<EventTrigger>( );

		EventTrigger.Entry entry = new EventTrigger.Entry( );
		entry.eventID = EventTriggerType.PointerEnter;
		entry.callback.AddListener( ( data ) => { OnEnter(data); } );

		EventTrigger.Entry exit = new EventTrigger.Entry( );
		exit.eventID = EventTriggerType.PointerExit;
		exit.callback.AddListener( ( data ) => { onExit(data); } );

		trigger.triggers.Add( entry );
		trigger.triggers.Add( exit );
	}
	public void OnPointerClick(PointerEventData eventData)
	{
		lastClickTime = Time.time;
		clickFunction (eventData);

	}
}


public class GUIScrollBar : NewMenuElement
{
	int nValue=0;
	int nMax=1;
	int nMin=1;
	bool h_v=false;
	GameObject hsbBtnLeft;
	GameObject hsbBtnRight;
	GameObject hsbBtnBG;
	GameObject hsbBtnKnob;
	public delegate void ButtonUpdate ();

	public ButtonUpdate publicUpdate=null;

	public GUIScrollBar(InGameGUI game,Rect rPos,int iMin,int iMax)
	{
		nMax = iMax;
		nMin = iMin;
		Pos = rPos;
		this.game = game;
		Draw ();
	}

	public override void Draw()
	{
		h_v = (Pos.width > Pos.height);
		if (h_v) {

			hsbBtnLeft = NewMenus.GUIButton (game, new GUIContent ("<"), game.MenuSkin.MainMenuBU, new Rect (Pos.xMin, Pos.yMin, Pos.height, Pos.height), delegate {
				nValue = Mathf.Max (0, nValue - 1);
				Update ();
			}, null);

			hsbBtnRight = NewMenus.GUIButton (game, new GUIContent (">"), game.MenuSkin.MainMenuBU, new Rect (Pos.xMax - Pos.height, Pos.yMin, Pos.height, Pos.height), delegate {
				nValue = Mathf.Min (nValue + 1, GetMax());
				Update ();
			}, null);

			new Rect (Pos.xMin + Pos.height, Pos.yMin, Pos.width - Pos.height * 2, Pos.height);
			hsbBtnBG = NewMenus.GUIButton (game,null, game.MenuSkin.MainMenuBU, new Rect (Pos.xMin + Pos.height, Pos.yMin, Pos.width - Pos.height * 2, Pos.height),delegate {

				float vMin = (Pos.xMin+Pos.height*1.5f)*Screen.width/1600;
				float vMax = (Pos.width-Pos.height*3f)*Screen.width/1600;

				int Value = Mathf.RoundToInt(((Input.mousePosition.x-vMin)/vMax)*GetMax());

				SetValue(Value);

			},null);
			hsbBtnBG.AddComponent<EventTrigger> ();

			EventTrigger.Entry onDrag = new EventTrigger.Entry( );
			onDrag.eventID = EventTriggerType.Drag;

			onDrag.callback.AddListener( ( data ) => { 

				float vMin = (Pos.xMin+Pos.height*1.5f)*Screen.width/1600;
				float vMax = (Pos.width-Pos.height*3f)*Screen.width/1600;

				int Value = Mathf.RoundToInt(((Input.mousePosition.x-vMin)/vMax)*nMax);

				SetValue(Value);

			} );

			hsbBtnBG.GetComponent<EventTrigger> ().triggers.Add( onDrag );

			hsbBtnKnob = NewMenus.GUIContent (game, new GUIContent (game.MenuSkin.MainMenuBU.normal.background), null, 
				new Rect (Pos.xMin + Pos.height, Pos.yMin, Pos.height, Pos.height));
		} else {
			hsbBtnLeft = NewMenus.GUIButton (game, new GUIContent ("^"), game.MenuSkin.MainMenuBU, new Rect (Pos.xMin, Pos.yMin, Pos.width, Pos.width), delegate {
				nValue = Mathf.Max (0, nValue - 1);
				Update ();
			}, null);

			hsbBtnRight = NewMenus.GUIButton (game, new GUIContent ("v"), game.MenuSkin.MainMenuBU, new Rect (Pos.xMin, Pos.yMax - Pos.width, Pos.width, Pos.width), delegate {
				nValue = Mathf.Min (nValue + 1, GetMax());
				Update ();
			}, null);

			hsbBtnBG = NewMenus.GUIButton (game, null, game.MenuSkin.MainMenuBU,new Rect (Pos.xMin, Pos.yMin + Pos.width, Pos.width, Pos.height - Pos.width * 2),delegate {

				float vMin = (Pos.yMin+Pos.height*1.5f)*Screen.height/900;
				float vMax = (Pos.height-Pos.height*3f)*Screen.height/900;

				int Value = Mathf.RoundToInt(((Input.mousePosition.y-vMin)/vMax)*GetMax());

				SetValue(Value);

			} , null);
			hsbBtnBG.AddComponent<EventTrigger> ();

			EventTrigger.Entry onDrag = new EventTrigger.Entry( );
			onDrag.eventID = EventTriggerType.Drag;
			onDrag.callback.AddListener( ( data ) => { 

				float vMin = (Pos.yMin+Pos.height*1.5f)*Screen.height/900;
				float vMax = (Pos.height-Pos.height*3f)*Screen.height/900;

				int Value = Mathf.RoundToInt(((Input.mousePosition.y-vMin)/vMax)*GetMax());

				SetValue(Value);

			} );

			hsbBtnBG.GetComponent<EventTrigger> ().triggers.Add( onDrag );

			hsbBtnKnob = NewMenus.GUIContent (game, new GUIContent (game.MenuSkin.MainMenuBU.normal.background), null, 
				new Rect (Pos.xMin, Pos.yMin + Pos.width, Pos.width, Pos.width));
		}
	}

	public void MoveToRect(Rect newPos)
	{
		Pos = newPos;

		h_v = (Pos.width > Pos.height);
		if (h_v) {
			NewMenus.RectToTransform (hsbBtnLeft.GetComponent<RectTransform> (),
				new Rect (Pos.xMin, Pos.yMin, Pos.height, Pos.height), false);

			NewMenus.RectToTransform (hsbBtnRight.GetComponent<RectTransform> (),
				new Rect (Pos.xMax - Pos.height, Pos.yMin, Pos.height, Pos.height), false);

			NewMenus.RectToTransform (hsbBtnBG.GetComponent<RectTransform> (),
				new Rect (Pos.xMin + Pos.height, Pos.yMin, Pos.width - Pos.height * 2, Pos.height), false);

			NewMenus.RectToTransform (hsbBtnKnob.GetComponent<RectTransform> (),
				new Rect (Pos.xMin + Pos.height+(Pos.width-Pos.height*3)*((float)nValue/(float)GetMax()), Pos.yMin, Pos.height, Pos.height),false);
		} else {
			NewMenus.RectToTransform (hsbBtnLeft.GetComponent<RectTransform> (),new Rect (Pos.xMin, Pos.yMin, Pos.width, Pos.width), false);

			NewMenus.RectToTransform (hsbBtnRight.GetComponent<RectTransform> (), new Rect (Pos.xMin, Pos.yMax - Pos.width, Pos.width, Pos.width), false);

			NewMenus.RectToTransform (hsbBtnBG.GetComponent<RectTransform> (), new Rect (Pos.xMin , Pos.yMin+ Pos.width, Pos.width, Pos.height - Pos.width * 2), false);

			NewMenus.RectToTransform (hsbBtnKnob.GetComponent<RectTransform> (),
				new Rect (Pos.xMin, Pos.yMin + Pos.width+(Pos.height-Pos.width*3)*((float)nValue/(float)GetMax()), Pos.width, Pos.width),false);
		}
	}

	public void Update()
	{
		if (h_v) {
			NewMenus.RectToTransform (hsbBtnKnob.GetComponent<RectTransform> (),
				new Rect (Pos.xMin + Pos.height+(Pos.width-Pos.height*3)*((float)nValue/(float)GetMax()), Pos.yMin, Pos.height, Pos.height),false);
		} else {
			NewMenus.RectToTransform (hsbBtnKnob.GetComponent<RectTransform> (),
				new Rect (Pos.xMin, Pos.yMin + Pos.width+(Pos.height-Pos.width*3)*((float)nValue/(float)GetMax()), Pos.width, Pos.width),false);
		}

		if (publicUpdate != null) {
			publicUpdate ();
		}
	}

	public void SetMax(int iMax,bool Reverse)
	{
		nMax = iMax;
		nValue = 0;
		if (Reverse) {
			nValue = nMax;
		}
		if (GetMax() <= 0) {
			EnableDisable (false);
		} else {			
			Update ();
		}
	}

	public void SetMin(int iMin)
	{
		nMin = iMin;
		nValue = 0;
	}

	public List<GameObject> GetGameObjects()
	{
		return new List<GameObject>{hsbBtnLeft,hsbBtnRight,hsbBtnBG,hsbBtnKnob};
	}

	public int GetValue()
	{
		return nValue;
	}

	public int GetMax()
	{
		return nMax-nMin;
	}

	public void SetValue(int toWhat)
	{
		nValue = Mathf.Clamp (toWhat, 0, GetMax());
		Update ();
	}

	public override void EnableDisable(bool toWhat)
	{
		hsbBtnLeft.SetActive (toWhat);
		hsbBtnRight.SetActive (toWhat);
		hsbBtnBG.SetActive (toWhat);
		hsbBtnKnob.SetActive (toWhat);
	}
}

public class NewMenuElement
{
	public Rect Pos;
	public List<GameObject> myStuff;
	public InGameGUI game;

	public virtual void Draw()
	{

	}

	public virtual void EnableDisable(bool toWhat)
	{
		foreach (GameObject myObj in myStuff) {
			myObj.SetActive (toWhat);

			if (toWhat) {
				myObj.transform.SetAsLastSibling ();
			}
		}
	}
}

public class NewMenuWindow
{
	public Rect Pos;
	public List<GameObject> myStuff=new List<GameObject>();
	public InGameGUI game;

	public NewMenuWindow()
	{

	}

	public NewMenuWindow(InGameGUI gDat, Rect rPos)
	{
		game = gDat;
		Pos = rPos;
	}

	public virtual void Draw()
	{

	}

	public virtual void Update()
	{

	}

	public void DrawBlank()
	{



		GameObject CloseBtn = NewMenus.GUIButton (game, new GUIContent (), new GUIStyle (), new Rect (0, 0, 1600f, 900f), delegate {
			Close ();
		}, null);
		CloseBtn.transform.SetAsFirstSibling ();
		myStuff.Add(CloseBtn);
	}


	public void DrawWindow(string Name)
	{
		DrawBlank();

		myStuff.Add(NewMenus.GUIWindow (game, game.MenuSkin.MainMenuBG, Pos));
		myStuff.Add (NewMenus.GUIContentBox (game, new GUIContent(Name), game.MenuSkin.MainMenuUI, new Rect (Pos.xMin + Pos.width / 4, Pos.yMin - 35f, Pos.width / 2f, 35f),5f).transform.parent.gameObject);

	}

	public bool isMouseOverRect()
	{
		Rect tPos = new Rect(
			Pos.xMin*Screen.width/NewMenus.ScreenResolution.x,
			Pos.yMin*Screen.width/NewMenus.ScreenResolution.x,
			Pos.width*Screen.width/NewMenus.ScreenResolution.x,
			Pos.height*Screen.width/NewMenus.ScreenResolution.x
		);
		Vector2 mPos = Input.mousePosition;

		return tPos.Contains(new Vector2(mPos.x,Screen.height-mPos.y));
	}

	public void DrawSingleWindow(string Name)
	{

		Close ();
		myStuff.Add(NewMenus.GUIWindow (game, game.MenuSkin.MainMenuBG, Pos));
		myStuff.Add (NewMenus.GUIContentBox (game, new GUIContent(Name), game.MenuSkin.MainMenuUI, new Rect (Pos.xMin + Pos.width / 4, Pos.yMin - 35f, Pos.width / 2f, 35f),5f).transform.parent.gameObject);

		GameObject CloseBtn = NewMenus.GUIButton (game, new GUIContent (), game.MenuSkin.MainMenuUI, new Rect (Pos.xMax-16f, Pos.yMin, 16f, 16f), delegate {
			//game.DrawMainMenus ();
		}, null);
		myStuff.Add(CloseBtn);

	}
	public bool amClosed=false;
	public virtual void Close()
	{if (!amClosed) {
			onClose ();
		}
		amClosed = true;
		foreach (GameObject myObj in myStuff) {			
			GameObject.Destroy (myObj);
		}
		myStuff.Clear ();
	}
	public virtual void onClose()
	{
		fClose (null);
	}
	public GUIButtonData.ButtonAction fClose = delegate {

	};
}

public class NewMenuDropList : NewMenuWindow
{
	public delegate void Valuetype(int Value);

	Valuetype[] Buttons;

	public string[] Entries;
	float Borders=0f;

	public NewMenuDropList(InGameGUI game,Rect Pos, List<string> OriginalList, List<Valuetype> ButtonData)
	{
		this.game = game;
		this.Borders = Pos.height;
		Entries = OriginalList.ToArray();
		Buttons = ButtonData.ToArray ();

		int Delta = 0;

		for (int Index = 0; Index < Entries.Length; Index++) {			

			string Zim = Entries [Index];
			if (Zim == "-") {
				Delta ++;
			}
		}

		this.Pos = new Rect
			(
				Mathf.Clamp(Pos.xMin,0,1600-Pos.width),
				Mathf.Clamp(Pos.yMin,0,900-(game.MenuSkin.MainMenuBU.fontSize+Borders)*ButtonData.Count),
				Pos.width,
				(game.MenuSkin.MainMenuBU.fontSize+Borders)*ButtonData.Count+Delta*Borders
			);

		if (Buttons.Length == 0) {
			Close ();
		} else {
			Draw ();
		}
	}

	public NewMenuDropList(InGameGUI game,Rect Pos, List<string> OriginalList, Valuetype ButtonData)
	{
		this.game = game;
		this.Borders = Pos.height;
		Entries = OriginalList.ToArray();
		Buttons = new Valuetype[]{ButtonData};

		int Delta = 0;

		for (int Index = 0; Index < Entries.Length; Index++) {			

			string Zim = Entries [Index];
			if (Zim == "-") {
				Delta ++;
			}
		}
		this.Pos = new Rect
			(
				Mathf.Clamp(Pos.xMin,0,1600-Pos.width),
				Mathf.Clamp(Pos.yMin,0,900-(game.MenuSkin.MainMenuBU.fontSize+Borders)*OriginalList.Count),
				Pos.width,
				(game.MenuSkin.MainMenuBU.fontSize+Borders)*OriginalList.Count+Delta*Borders
			);

		if (Buttons.Length == 0) {
			Close ();
		} else {
			Draw ();
		}
	}

	public override void Draw()
	{		
		myStuff = new List<GameObject> ();

		myStuff.Add(NewMenus.GUIButton (game, null, new GUIStyle(), new Rect (0,0,1600,900), delegate {
			Close ();
		},null));

		myStuff.Add(NewMenus.GUIWindow(game,game.MenuSkin.MainMenuBG,Pos));

		int Delta = 0;

		for (int Index = 0; Index<Entries.Length; Index++) {			

			string Zim = Entries[Index];
			if (Zim == "-") {
				Delta++;
			} else {
				int Value = Index - Delta;
				myStuff.Add (NewMenus.GUIButton (game, new GUIContent (Zim), game.MenuSkin.MainMenuBU,
					new Rect (Pos.xMin, Pos.yMin + (game.MenuSkin.MainMenuBU.fontSize + Borders) * Value+Delta*Borders, Pos.width, game.MenuSkin.MainMenuBU.fontSize + Borders), 
					delegate {

						Close ();
						if (Buttons.Length==1) {

							Buttons [0] (Value);
						}
						else {
							Buttons [Value] (Value);

						}
					}, null));
			}
		}
	}
}