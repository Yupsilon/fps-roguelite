﻿using System;
using UnityEngine;
using System.Collections.Generic;

	public class entityPlayer:entityActor
	{
	public entityTurret turret;
	public int Character=0;
	public int SelectedWeapon=0;
	public int[] Ammo;
	public int[] Clips;
	float lastWeaponChangeTimer = 0;

	public propertyWeapon[] Weapons;

	public override float getHeight()
	{
		switch (Character)
		{
		case 0://Cadet
			return 0.4f;
		case 1://Spaz
			return 0.36f;
		case 2://Kid
			return 0.24f;
		case 3://Robot
			return 0.44f;
		case 4://Creature
			return 0.46f;
		case 5://Angel
			return 0.42f;
		}
		return 0.4f;
	}

	public override bool isPlayer()
	{
		return true;
	}

	public override void onCreated ()
	{
		Radius = 0.25f;
		Side = 1;

		gameObject = new GameObject ();
		gameObject.name = GetName ();
		gameObject.transform.localScale *= GetSpriteScale ();

		gameObject.AddComponent < SpriteRenderer> ();

		Weapons= new propertyWeapon[5];
		Weapons [0] = propertyWeapon.Random (propertyWeapon.WeaponType.PulsePistol,false);
		Weapons [1] = propertyWeapon.Random (propertyWeapon.WeaponType.PulsePistol,true);
		Weapons [2] = propertyWeapon.Random (propertyWeapon.WeaponType.Rocket,true);
		Weapons [3] = propertyWeapon.Random (propertyWeapon.WeaponType.Rocket,false);
		Weapons [4] = propertyWeapon.Random (propertyWeapon.WeaponType.Grenade,false);

		DrawSprites ("Players/Player" + Character);
		base.onCreated ();

		turret = new entityTurret (game, this);
	}

	public entityPlayer (StateInGame gData, Vector2 Pos)
	{
		game = gData;
		Position = Pos;

		squirtColor = GetColor();
		onCreated ();
	}

	public Color GetColor()
	{
		return Color.red;
	}

	public override float GetElasticity ()
	{
		return 0;
	}

	public override string GetName ()
	{
		return "Player";
	}

	public float GetDashTime()
	{
		switch (Character)
		{
		case 0://Cadet
			return 0.2f;
		case 1://Spaz
			return 1.0f;
		case 2://Kid
			return 3.0f;
		case 3://Robot
			return 0.0f;
		case 4://Creature
			return 4.00f;
		case 5://Angel
			return 2.00f;
		}
		return 0.4f;
	}

	public void DoDash()
	{
		if (!HasModifier ("dashCD") && !HasModifier ("Jump")) {

			switch (Character)
			{
			case 0://Cadet
				Vector2 Direction = new Vector2 (
					                   Input.GetAxisRaw ("Horizontal"),
					                   Input.GetAxisRaw ("Vertical")
				                   );

				if (Direction != Vector2.zero) {
					Velocity.x = Direction.y * GetDashSpeed () * Mathf.Sin (Rotation / 180 * Mathf.PI) + Direction.x * GetDashSpeed () * Mathf.Sin ((Rotation + 90) / 180 * Mathf.PI);
					Velocity.y = Direction.y * GetDashSpeed () * Mathf.Cos (Rotation / 180 * Mathf.PI) + Direction.x * GetDashSpeed () * Mathf.Cos ((Rotation + 90) / 180 * Mathf.PI);


					AddModifier (game, new propertyModifier ("Dash", propertyModifier.Allignment_neutral, GetDashTime (), new int[]{ }, new float[][]{ new float[] {
							propertyModifier.property_friction,
							0
						} }), 0);
					AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () * 5, new int[]{ }), 0);
				}
				break;

			case 1://Spaz
				AddModifier (game, new propertyModifier ("Dash", propertyModifier.Allignment_neutral, GetDashTime (), new int[]{ }, new float[][]{ new float[] {
						propertyModifier.property_time,
						0.5f
					} }), 0);
				AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () * 5, new int[]{ }), 0);
				break;
			case 2://Kid

				AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () , new int[]{ }), 0);
				break;
			case 3://Robot
				FireWeapon(SelectedWeapon);
				break;
			case 4://Creature
				AddModifier (game, new propertyModifier ("Dash", propertyModifier.Allignment_neutral, GetDashTime (), new int[]{ propertyModifier.modstate_Flying,propertyModifier.modstate_Invulnerable,propertyModifier.modstate_Disarmed,propertyModifier.modstate_Unstoppable }, new float[][]{ }), 0);
				AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () * 5, new int[]{ }), 0);
				break;
			case 5://Angel
				AddModifier (game, new propertyModifier ("Dash", propertyModifier.Allignment_neutral, GetDashTime (), new int[]{ propertyModifier.modstate_Invulnerable }, new float[][]{ }), 0);
				AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () * 5, new int[]{ }), 0);
				break;
			} 
		}
	}

	public void FireWeapon(int WeaponID){
		if (CanAttack() && Weapons[WeaponID]!=null){
			{
				turret.Rotation = Mathf.Atan2 (Input.GetAxis ("Attack1"), Input.GetAxis ("Attack2")) * 180f / Mathf.PI;

				Weapons[WeaponID].Fire (game, this,turret.Rotation);

				/* UNUSED if (HasModifier("reload")
				){
					propertyModifier turrmod = new propertyModifier ("Animate", 0, GetModifierByName ("reload").Duration / turret.MaxFrames, new int[0]);
					turret.AddModifier (game,turrmod , 2);
					turrmod.AddFunction (propertyModifier.actions.think, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						turret.Update();	
					});
					turrmod.thinkInterval = 0.2f;
					}*/
				}


			}
	}
	public override float GetFriction()
	{
		if (Character == 1) {
			return base.GetFriction ()*0.5f;
		}
		return base.GetFriction ();
	}

	public float GetMoveSpeed()
	{
		return 8;
	}

	public float GetDashSpeed()
	{
		return 6;
	}

	public void HandleInput()
	{
		if (CanAct ()) {
			Vector2 vSpeed =new Vector2(Input.GetAxisRaw ("Horizontal") * GetMoveSpeed(),Input.GetAxisRaw ("Vertical") * GetMoveSpeed());

			SetAcceleration(vSpeed.x , vSpeed.y);
			if (vSpeed!=Vector2.zero)
			{
				if (!HasModifier("Animate"))
				{
					AddModifier (game, new propertyModifier ("Animate",propertyModifier.Allignment_neutral, 1f/GetMoveSpeed(), new int[]{ }, new float[][]{}), 2);
				}
			}

			if (new Vector2(Input.GetAxis ("Attack1"),Input.GetAxis ("Attack2")).magnitude!=0) {


				FireWeapon (SelectedWeapon);
			}
			if (Input.GetAxisRaw ("ChangeWeapon") != 0) {

				if (lastWeaponChangeTimer < Time.time) {
					lastWeaponChangeTimer = Time.time + 0.3f;

					ChangeWeapon ();
				}
				//UNUSED DoDash ();
			}
		} else {
			Acceleration = Vector2.zero;}
	}

	public override Vector2 GetFlight()
	{
		return new Vector2 (-1, 0);
	}

	public override void Update ()
	{
		HandleInput ();
		base.Update ();
		turret.Update ();
	}

	public float GetAirTime(){
		return 0.5f;
	}

	public override int GetFacing()
	{
		if (HasModifier ("dead")) {
			return Animation_Death+4;

		} else {

			float rAngle = (Rotation+45 ) % 360;

			if (rAngle < 0) {
				rAngle = 360 + rAngle;
			}

			int final = Mathf.FloorToInt ((rAngle) / 360f * MaxSides);

			if (HasModifier ("dash")) {
				final += 4;

			}
			return final;
		}
	}

	public void ChangeWeapon()
	{
		SelectedWeapon++;
		while (Weapons [SelectedWeapon].Clip == 0 && !Weapons [SelectedWeapon].HasInfiniteAmmo ()) {
			SelectedWeapon++;
		}
	}

	/* UNUSED	public override float getPuddleRadius ()
	{
		return 0.5f;
	}*/

}