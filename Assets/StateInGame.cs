﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateInGame {

		public ArrayList Index=new ArrayList();
	public ArrayList IndexName=new ArrayList();
	public Camera MainCamera;
	public GameResources Memory;
	public float gameTime=0;

	public propertyFloor floor;
	public entityBase gamePlayer;
	public List<entityBase> gameEntities;

	public bool gamepaused = false;
	public List<SpriteRenderer> Colchangers;
	// Use this for initialization
	public  StateInGame (GameResources mem) {
		Memory = mem;

		PrepTheCamera ();

		gameEntities = new List<entityBase> ();
		DrawTheWorld.DrawSmallStage (this,"Test");
		gamePlayer = new entityPlayer (this, getLimits().center);
		gameTime = 0;

		AddEntity(gamePlayer);
		AddEntity(entityCreep.MakeFromEntityData(this,0,Vector2.one*7));
		/*entityCritter.MakeFromEntityData(this,0, new Vector2 (3, 3));
		entityCritter.MakeFromEntityData(this,1, new Vector2 (4, 3));*/

		float timeSum = 0;
		Timer gameTimer = Timer.Create ("gameTimer", Time.fixedDeltaTime,
			delegate {

				while (timeSum<Time.deltaTime){
					Update();

					timeSum+=Time.deltaTime;}

				timeSum-=Time.deltaTime;
				return Time.deltaTime;
			}, delegate{
				return 0;
			});
		gameTimer.running = true;
		ChangeWorldColor (Color.red);
	}

	public entityBase AddEntity(entityBase ent)
	{
		gameEntities.Add(ent);
		return ent;
	}

	// Update is called once per frame
	void Update () {
		gameTime += Time.deltaTime;

		List<entityBase> irrelevantEntities = new List<entityBase> ();
		for (int Zim = 0; Zim < gameEntities.Count; Zim++) {
			gameEntities [Zim].Update ();
			if (gameEntities [Zim].isDiscardable ()) {
				irrelevantEntities.Add (gameEntities [Zim]);}		}
		foreach (entityBase Zyzyx in irrelevantEntities) {
			gameEntities.Remove (Zyzyx);
			GameObject.Destroy (Zyzyx.gameObject);
		}
	}

	public float GetSpeed(){
		return Time.fixedDeltaTime;}

	public Rect getLimits()
	{
		return new Rect (0, 0, 14, 14);
	}

	public bool isOutOfBounds(Vector2 pos)
	{
		return !getLimits ().Contains (pos);
	}

	public void PrepTheCamera()
	{
		MainCamera =  new GameObject().AddComponent<Camera>();		
		MainCamera.orthographic = true;
		MainCamera.allowHDR = false;
		MainCamera.gameObject.name = "Game Camera";
		MainCamera.clearFlags = CameraClearFlags.Color;
		MainCamera.backgroundColor = Color.black;

		MainCamera.orthographicSize = getLimits().width/2f+1;
		MainCamera.transform.position = new Vector3 (getLimits().center.x,getLimits().center.y, -getLimits().width);
		/*MainCamera.cullingMask &= ~(1 << Game.LayerMinimap);
		MainCamera.cullingMask &= ~(1 << Game.LayerInvisible);*/

	
		MainCamera.depth = 1;
	}

	public void generatePuddle(Vector2 worldVector, float rad, int nExplosions,Color32 col, int playerID)
	{
		float small = 1;
		for (int Gir = 1; Gir < nExplosions; Gir++) {
			small*=0.95f;
		}

		float real = (1 - small);

		List<entitySpecialEffect> explosions = new List<entitySpecialEffect> ();
		for (int nPoof = 0; nPoof < nExplosions; nPoof++) {

			Vector3 center = new Vector3 (
				worldVector.x+(1-UnityEngine.Random.value*2)*real*real*rad,
				worldVector.y+(1-UnityEngine.Random.value*2)*real*real*rad
			                 );
			generatePuddle (center, small*rad*2f, col, playerID);
		}
	}

	public void generatePuddle(Vector2 worldVector, float r,Color32 col, int playerID)
	{
		float multi = propertyFloor.FloorMulti;
		floor.generatePuddle(Mathf.FloorToInt(worldVector.x*DrawTheWorld.DefaultTileSize*multi),Mathf.FloorToInt(worldVector.y*DrawTheWorld.DefaultTileSize*multi),(int)(r*multi),col,playerID);
		floor.Update();
	}

	/*
	void MoveCamera(float X,float Y,float Z)
	{
		if (MovingArmies.Count == 0 && CameraOrder.x < 0 && CameraOrder.y>0) {

			GameCamera.transform.position = new Vector3 (
				GameCamera.transform.position.x + X * Game.CameraSpeed, 
				GameCamera.transform.position.y + Y * Game.CameraSpeed,
				GameCamera.transform.position.z
			);
		} 
		if (CameraOrder.z != -1) {

			FocusCamera (CameraOrder.z + Z * Game.CameraSpeed * 10f);
		}
		else {
			FocusCamera (GameCamera.orthographicSize + Z * Game.CameraSpeed * 10f);
		}
	}

	public void FocusCamera(Vector3 Order)
	{
		CameraOrder = new Vector3 ( Order.x+1/2f, 0 - Order.y-1/2f, Order.z);
		CameraOrigin = new Vector3 (GameCamera.transform.position.x,
			GameCamera.transform.position.y,
			GameCamera.orthographicSize);
		SetOrder (GetCameraTime());
	}

	public void FocusCamera(float x, float y)
	{
		CameraOrder.x = x * Game.iTileSize+1/2f;
		CameraOrder.y =-y*Game.iTileSize-1/2f;

		CameraOrigin.x = GameCamera.transform.position.x;
		CameraOrigin.y = GameCamera.transform.position.y;

		SetOrder (GetCameraTime());
	}

	public void FocusCamera(float Order)
	{
		CameraOrder.z=Order;
		CameraOrigin.z = GameCamera.orthographicSize;

		SetOrder (GetCameraTime());
	}

	public void SetOrder(float dur)
	{
		CameraTime = new Vector2 (dur/Time.smoothDeltaTime,0);

		if (CameraOrder.z == -1) {
			CameraOrder.z = GameCamera.orthographicSize;
		}
		CameraOrder=  new Vector3(
			(CameraOrder.x),
			(CameraOrder.y),
			Mathf.Min (Mathf.Max (
				CameraOrder.z,
				5),
				Mathf.Min (Scenario.GetSize (true), Scenario.GetSize (false)))
		);

	}
	bool minimapMode=false;
	public void ChangeCameraMode(bool zoomOut)
	{
		if (zoomOut != minimapMode) {
			minimapMode = zoomOut;
			if (zoomOut) {
				GameCamera.cullingMask |= (1 << Game.LayerMinimap);
				GameCamera.cullingMask ^= (1 << Game.LayerMain);

			} else {
				GameCamera.cullingMask ^= (1 << Game.LayerMinimap);
				GameCamera.cullingMask |= (1 << Game.LayerMain);

			}
		}
	}

	public Vector3 CameraOrder = Vector3.zero;
	public Vector3 CameraOrigin = Vector3.zero;
	public Vector2 CameraTime = Vector2.zero;

	public void AdjustCamera()
	{
		float H = GameCamera.orthographicSize;

		if (CameraOrder.z >= 0){

			H = Mathf.Min(H,CameraOrder.z);
		}

		if (CameraTime.x > 0 && CameraTime.y > 0) {
			if (CameraOrder.x != -1 && CameraOrder.y != 1 ) {
				GameCamera.transform.position = new Vector3 (
					GameCamera.transform.position.x + (CameraOrder.x - CameraOrigin.x) / CameraTime.x,
					GameCamera.transform.position.y + (CameraOrder.y - CameraOrigin.y) / CameraTime.x,
					GameCamera.transform.position.z);
			}
		}
		if (CameraOrder.z != -1) {
			GameCamera.orthographicSize = GameCamera.orthographicSize + (CameraOrder.z - CameraOrigin.z) / CameraTime.x;
		}

		if (CameraTime.y >= CameraTime.x) {
			ClearOrder ();
		} else {CameraTime.y++;}
		ChangeCameraMode (GameCamera.orthographicSize > 10);

		float W = H*GameCamera.aspect;
		float fX = GameCamera.transform.position.x;
		float fY = GameCamera.transform.position.y;

		if (fX<W - 1 / 2f || fX>Scenario.GetSize(true) - W - 1 / 2f) {
			fX = Mathf.Clamp (fX,
				W - 1 / 2f,
				Scenario.GetSize(true) - W - 1 / 2f);
		}
		if (fY<-Scenario.GetSize(false) + H + 1 / 2f || fY>-H + 0.5f){
			fY = Mathf.Clamp (fY,
				-Scenario.GetSize(false) + H + 1 / 2f,
				-H + 0.5f);
		}

		if (W>= Scenario.GetSize(true)/ 2f) {
			fX = Scenario.GetSize(true) / 2f;

		}

		if (H >= Scenario.GetSize(false)/ 2f) {
			fY = -Scenario.GetSize(false) / 2f+1/2f;
		}
		GameCamera.transform.position = new Vector3 (fX, fY, Game.CameraLevel);



	}*/

	public List<entityActor> getEntitiesInRadius(Vector2 center, float rad)
	{
		List<entityActor> final = new List<entityActor> ();
		foreach (entityBase Zim in gameEntities)
		{
			if (Zim as entityActor!=null && ((Vector2)Zim.Position - center).magnitude < rad) {
				final.Add (Zim as entityActor);
			}
		}
		return final;
	}

	public void ChangeWorldColor(Color nCol)
	{
		foreach (SpriteRenderer Zim in Colchangers) {
			Zim.color = nCol;
		}
	}
}
