﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityTurret:entityActor
{
	public entityPlayer parent;



	public override void onCreated ()
	{
		Radius = 0.25f;
		Side = 1;

		gameObject = new GameObject ();
		gameObject.name = GetName ()+" head";
		gameObject.transform.localScale *= GetSpriteScale ();

		gameObject.AddComponent < SpriteRenderer> ();


		DrawSprites ("Players/Player" + parent.Character+"_head");
		base.onCreated ();
	}

	public entityTurret (StateInGame gData, entityPlayer Owner)
	{
		game = gData;

		parent = Owner;
		squirtColor = Owner.GetColor();
		onCreated ();


	}

	public override float GetElasticity ()
	{
		return 0;
	}

	public override string GetName ()
	{
		return "Player Head";
	}



	public override void Update ()
	{
		if (HasModifier ("Animate")) {
			propertyModifier Mod = GetModifierByName ("Animate");

			float Modifier = Mathf.Abs(Mod.GetModifierDuration (game) / Mod.totDuration);
			fFrame = Mathf.Clamp((MaxFrames ) * Modifier,0,(MaxFrames - 1));
		} else {
			fFrame = 0;
		}

		Position= new Vector3(parent.Position.x,parent.Position.y,parent.getHeight()-0.05f);

		int iSprite = GetFacing () * MaxFrames + Mathf.FloorToInt (fFrame);
		gameObject.GetComponent<SpriteRenderer> ().sprite =SpriteData[iSprite] ;
		Draw ();
	}

	public override int GetFacing()
	{
			float rAngle = (Rotation+45 ) % 360;

			if (rAngle < 0) {
				rAngle = 360 + rAngle;
			}

			int final = Mathf.FloorToInt ((rAngle) / 360f * (MaxSides*2));

			return final;
		
	}

}