﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityCreep: entityActor
{


	public static string[] textureNames = new string[]{
		"Testy",
		"Testy",
		"Testy"
	};

	public override bool TakeProjectiles ()
	{
		return true;
	}

	public static entityCreep MakeFromEntityData(StateInGame game, int entity, Vector2 Position)
	{
		entityCreep Owner = new entityCreep(game, Position,textureNames[entity]);

		switch (entity) {

		case 0: // Dasher Randomly

			float DashTime = 0.33f;
			float DashSpeed = 2f;
			float ThinkInterval = 3f;

			Owner.Brain = new entityAI (
				Owner,
				ThinkInterval);

			Owner.Brain.AddFunction(entityAI.nThink,
				delegate {
					float Rotation = UnityEngine.Random.Range(0,360);
					Owner.Rotation=Rotation;

					Owner.Velocity = new Vector2(
						DashSpeed * Mathf.Sin (Rotation / 180 * Mathf.PI),
						DashSpeed * Mathf.Cos (Rotation / 180 * Mathf.PI)
					);

					propertyModifier dashMod=new propertyModifier ("Dash",propertyModifier.Allignment_neutral, DashTime, new int[]{ }, new float[][]{new float[] {propertyModifier.property_friction,0}});
					/* UNUSED dashMod.AddFunction(propertyModifier.actions.expire, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						
						game.generatePuddle (Owner.Position, 1f*DrawTheWorld.DefaultTileSize/2f,9, Owner.squirtColor, 1);

					});*/

					Owner.AddModifier (Owner.game, dashMod, 0);
					Owner.AddModifier (Owner.game, new propertyModifier ("Animate",propertyModifier.Allignment_neutral, DashTime, new int[]{ }, new float[][]{}), 0);
				}
			);

			break;
		case 1: // Headless Bombken - Walk randomly

			float WalkSpeed = 0.4f;
			ThinkInterval = 2f + UnityEngine.Random.Range (0, 3);

			Owner.Brain = new entityAI (
				Owner,
				ThinkInterval);

			Owner.Brain.AddFunction (entityAI.nThink,
				delegate {
					float Rotation = UnityEngine.Random.Range (-180, 180) + Owner.Rotation;
					Owner.Rotation = Rotation;

					Owner.Acceleration = new Vector2 (
						WalkSpeed * Mathf.Sin (Rotation / 180 * Mathf.PI),
						WalkSpeed * Mathf.Cos (Rotation / 180 * Mathf.PI)
					);
					Owner.AddModifier (Owner.game, new propertyModifier ("Base",propertyModifier.Allignment_neutral, -1, new int[]{ }, new float[][]{ new float[] {
							propertyModifier.property_elasticity,
							0
						} }), 0);
					Owner.AddModifier (Owner.game, new propertyModifier ("Animate",propertyModifier.Allignment_neutral, -0.5f/WalkSpeed, new int[]{ }, new float[][]{}), 2);
				}
			);

			Owner.Brain.AddFunction (entityAI.nCollide,delegate {

				Owner.Brain.ExecuteFunction(entityAI.nThink);
			});
			break;
		case 2: // Stands still
			Owner.Brain = new entityAI (
				Owner,
				9999999f);
			//Stand still, maybe shoot liek a turret
			Owner.AddModifier(game, new propertyModifier("Base",propertyModifier.Allignment_neutral,-1,null, new float[][]{ new float[]{propertyModifier.property_mass,3}}),2);
			break;
		}
	

		return Owner;
	}

	public override void onCollide (float X, float Y)
	{
		Brain.ExecuteFunction (entityAI.nCollide);
	}

	public override void onCollide (entityBase target)
	{
		base.onCollide (target);
		Brain.ExecuteFunction (entityAI.nHit);
		if (target.isPlayer ()) {

			Brain.ExecuteFunction (entityAI.nAttack);
		}

	}

	public override void onCreated ()
	{
		gameObject = new GameObject ();
		gameObject.name = GetName ();
		gameObject.transform.localScale *= GetSpriteScale ();

		gameObject.AddComponent < SpriteRenderer> ();

		base.onCreated ();
	}

	public entityCreep  (StateInGame gData, Vector2 Pos, string Texture)
	{
		game = gData;
		game.gameEntities.Add (this);
		Position = Pos;

		Radius = 0.25f;
		onCreated ();
		DrawSprites ("Enemies/" + Texture);
	}

	public override bool isDiscardable()
	{
		return isDead;
	}

	public override void Die ()
	{
		float dTime = 3;

		AddModifier (game,
			new propertyModifier ("dead",propertyModifier.Allignment_neutral, dTime, new int[]{propertyModifier.modstate_Stunned},new float[][] {},delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
				target.isDead=true;
			}), 1);
		AddModifier (game, new propertyModifier ("Animate",propertyModifier.Allignment_neutral, dTime, null, null), 0);

	}

	public entityAI Brain;

	public override void Update ()
	{
		if (Brain != null) {
			Brain.Update ();
		}

		base.Update ();
	}
}
