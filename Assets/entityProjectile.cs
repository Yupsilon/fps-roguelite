﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityProjectile:entitySpecialEffect
{
	public propertyModifier controller;
	public int mTargets=0;
	public entityBase Caster;
	public Vector3 Lobbed = Vector3.zero;
	public List<entityBase> HitUnits;
	float height = 0f;

	public entityProjectile  (StateInGame gData, Vector3 Pos, Color32 col, float expiry)
		{
			game = gData;
			game.gameEntities.Add (this);
			Position = Pos;
		height = Pos.z;

			gameObject = new GameObject ();
		gameObject.name = GetName ();
		gameObject.transform.localScale *= GetSpriteScale ();
		squirtColor = col;

		gameObject.AddComponent < SpriteRenderer> ()	;
		gameObject.GetComponent < SpriteRenderer> ().color=col;
		HitUnits = new List<entityBase> ();

		controller = new propertyModifier ( GetName (),0,0,new int[0]);
		if (expiry >= 0) {
			LifeTime = game.gameTime + expiry;

			Lobbed.z = game.gameTime;
		}
	}

	public override float GetSpriteScale ()
	{
		return 2f;
	}

	public override void Tick ()
	{
		
			Vector2 Destination = new Vector2 (
				Position.x+Velocity.x*GetSpeed(),
				Position.y+Velocity.y*GetSpeed()
			);

		HitScan Data = new HitScan (game, Caster, Position, Destination, Radius, mTargets-HitUnits.Count,true,-1);

		Position = Data.GetEndPosition (true);

		if (Lobbed.y != 0) {
			Position.z = Lobbed [0] *
			(game.gameTime - Lobbed.z) / (LifeTime - Lobbed.z) +
			Mathf.Sin ((game.gameTime - Lobbed.z) / (LifeTime - Lobbed.z) * Mathf.PI);
		} else if (Data.HitUnits.Count>0)
		{
			foreach (entityActor Zim in Data.HitUnits) {

				onCollide (Zim);
				HitUnits.Add (Zim);
			}
		}

		if (Data.ded) {
			Die ();
		}

		Draw ();
	}

	public override float GetElasticity ()
	{
		return 1;
		/* UNUSED if (Lobbed.y != 0) {
			return 1;

		}
		return base.GetElasticity ();*/
	}

	public override void Update ()
	{
		fFrame += Time.deltaTime*fFPS;
		if (fFrame >= SpriteData.Count) {
			fFrame = 0;
		}
		if (LifeTime >= 0) {
			if (LifeTime < game.gameTime) {
				Die ();
			}
		}

		base.Update ();
	}

				public static entityProjectile LaunchProjectile(StateInGame gData, 
		entityBase Caster, int Targets,
		Vector3 Pos, float time,
		float Vel, float Ang,float Rad,
		string nSprite,Color32 col)
	{
		entityProjectile Launcher = new entityProjectile (gData,Pos, col,time);
		Launcher.Velocity = new Vector2 (
			Vel*Mathf.Sin(Ang/180*Mathf.PI),
			Vel*Mathf.Cos(Ang/180*Mathf.PI)
		);
		Launcher.mTargets = Targets;
		Launcher.Caster = Caster;
		Launcher.Lobbed.x = Pos.z+Caster.getHeight()/2f;
		Launcher.Radius = Rad;	

		Launcher.SpriteData = new List<UnityEngine.Sprite> ();
		Texture2D BaseTexture = DrawTheWorld.LoadTex ("Projectiles/" + nSprite);

		Launcher.gameObject.name = BaseTexture.name;
		for (int Zim = 0; Zim < Mathf.FloorToInt (BaseTexture.width / BaseTexture.height); Zim++) {
			Launcher.SpriteData.Add (Sprite.Create (BaseTexture,
				new Rect (0 + BaseTexture.height * Zim, 0, BaseTexture.height, BaseTexture.height),
				Vector2.one/2f	));
		}


		return Launcher;
	}

	public static entityProjectile LaunchProjectileWithFuncs(StateInGame gData,
		entityBase Caster, int Targets,
		Vector3 Pos, float time,
		float Vel, float Ang,float Rad,
		string nSprite, Color32 col,
		propertyModifier.ModifierAction onCollide, 
		propertyModifier.ModifierAction onExpire)
	{
		entityProjectile Launcher = LaunchProjectile (gData,Caster,Targets,Pos,time, Vel, Ang, Rad, nSprite,col);
		Launcher.controller.AddFunction (propertyModifier.actions.expire, onExpire);
		Launcher.controller.AddFunction (propertyModifier.actions.collide, onCollide);
		return Launcher;
	}

	public override void onCollide (entityBase target)
	{
		controller.FireFunction (propertyModifier.actions.collide,target as entityActor,this);

	} 

	public override void Die()
	{
		controller.FireFunction (propertyModifier.actions.expire,this);
		if (!controller.dead) {
			controller.Die (false);
		}
		base.Die ();
	}

	public override void Draw ()
	{
		base.Draw ();
		float realH = getHeight();

		if (LifeTime >= 0) {
			if (LifeTime - game.gameTime<0.5f) {
				realH*=(LifeTime - game.gameTime)*2;
			}
		}

		gameObject.transform.position = new Vector3(Position.x,Position.y+realH*Mathf.Sin(Mathf.PI/2f),-3);
		gameObject.transform.rotation = Quaternion.Euler (0, 0, Mathf.Atan2(Velocity.y,Velocity.x)*180/Mathf.PI);
	}

	public override float getHeight ()
	{
		return height;
	}

	public void explode(string explosion, float radius)
	{
		Vector2 centerexplosion=(Vector2) Position - Velocity.normalized * Radius;

		entitySpecialEffect.MakeExplosion (game,centerexplosion, 0, 0, "explosion_default", false,-1,squirtColor);
		foreach (entityBase Zim in game.getEntitiesInRadius(centerexplosion,radius)) {
			controller.FireFunction (propertyModifier.actions.boom, Zim as entityActor,this);
		}
	}
}