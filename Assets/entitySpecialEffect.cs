using System;
using UnityEngine;
using System.Collections.Generic;

public class entitySpecialEffect:entityBase
{
	public float LifeTime = -1;

	public float fFPS=1;

	public Vector3 fixedRot=Vector3.zero-Vector3.one;

	public override bool TakeProjectiles ()
	{
		return false;
	}

	public static entitySpecialEffect MakeExplosion (StateInGame gData, Vector2 Pos, float expiry,float height,string nSprite,bool Looping,float Rot,Color c)
	{
		entitySpecialEffect Boom = new entitySpecialEffect ();

		Boom.game = gData;
		gData.gameEntities.Add (Boom);
		Boom.Position = Pos;

		Boom.gameObject = new GameObject ();
		Boom.gameObject.AddComponent < SpriteRenderer> ().color=c;

		Boom.SpriteData = new List<UnityEngine.Sprite> ();
		Texture2D BaseTexture = DrawTheWorld.LoadTex ("Effects/" + nSprite);

		Boom.gameObject.transform.localScale *= Boom.GetSpriteScale ();
		Boom.gameObject.name = BaseTexture.name;
		for (int Zim = 0; Zim < Mathf.FloorToInt (BaseTexture.width / BaseTexture.height); Zim++) {
			Boom.SpriteData.Add (Sprite.Create (BaseTexture,
				new Rect (0 + BaseTexture.height * Zim, 0, BaseTexture.height, BaseTexture.height),
				Vector2.one/2f));
		}

		if (Rot >= 0) {
			Boom.fixedRot = new Vector3(0,90 * Rot,0);
			Boom.Position += new Vector3 (
				Mathf.Sin (Rot * Mathf.PI / 2) / 5f,
				Mathf.Cos (Rot * Mathf.PI / 2) / 5f,0
			);
		}

		Boom.LifeTime = gData.gameTime + expiry;

		if (!Looping) {
			Boom.fFPS = 1 / expiry;
		}
		return Boom;
	}

	public static entitySpecialEffect MakeGunshot (StateInGame gData, Vector2 Pos, float expiry,float height,string nSprite,bool Looping,float Rot,Color c)
	{
		entitySpecialEffect Boom = MakeExplosion(gData,Pos,expiry,height,nSprite,Looping,Rot,c);

		Boom.Position.z+=-Boom.gameObject.transform.localScale.x/4f+height;
		return Boom;
	}

	public static entitySpecialEffect MakeBeam (StateInGame gData, Vector3 Start, Vector3 End, float expiry,string nSprite,bool Looping)
	{
		Texture2D BaseTexture = DrawTheWorld.LoadTex ("Projectiles/" + nSprite);
		entitySpecialEffect Boom = new entitySpecialEffect ();

		Boom.game = gData;
		gData.gameEntities.Add (Boom);
		Boom.Position = (Start+End)/2;

		Boom.gameObject = CreatePlane.Create(
			1,1,BaseTexture.width/80f,(End-Start).magnitude,CreatePlane.Orientation.Horizontal,CreatePlane.AnchorPoint.Center,true,"Plane"

		);
		Boom.fixedRot = new Vector3(
			Mathf.Atan2(End.y-Start.y,End.z-Start.z)*180f/Mathf.PI-90f,
			Mathf.Atan2(End.x-Start.x,End.y-Start.y)*180f/Mathf.PI,
			0
		);

		Boom.SpriteData = new List<UnityEngine.Sprite> ();
		Boom.gameObject.name = BaseTexture.name;
		for (int Zim = 0; Zim < Mathf.FloorToInt (BaseTexture.width / BaseTexture.height); Zim++) {
			Boom.SpriteData.Add (Sprite.Create (BaseTexture,
				new Rect (0 + BaseTexture.height * Zim, 0, BaseTexture.height, BaseTexture.height),
				new Vector2(0.5f,0f)));
		}
		
		CreatePlane.ApplySprite (Boom.gameObject.GetComponent<MeshRenderer> (),Boom.SpriteData[0]);


		Boom.LifeTime = gData.gameTime + expiry;

		if (!Looping) {
			Boom.fFPS = 1 / expiry;
		}
		return Boom;
	}

	public override void Draw ()
	{
		base.Draw ();
		if (gameObject.GetComponent<SpriteRenderer> () != null) {
			
			gameObject.GetComponent<SpriteRenderer> ().sprite = SpriteData [Mathf.FloorToInt (fFrame)];
		} else if (gameObject.GetComponent<MeshRenderer> () != null) {

			CreatePlane.ApplySprite (gameObject.GetComponent<MeshRenderer> (),SpriteData [Mathf.FloorToInt (fFrame)]);
		} 
	}


	public override void Update ()
	{
		fFrame += Time.deltaTime*fFPS;
		if (fFrame >= SpriteData.Count) {
			fFrame = 0;
			if (LifeTime < game.gameTime) {
				Die ();
			}
		}

		Move ();
	}

	public override float GetFriction(){

		return 0;}

	public override bool isDiscardable()
	{
		return isDead;
	}

	public override Vector2 GetFlight ()
	{
		return new Vector2(-1,0);
	}

	public override bool CanAct ()
	{
		return false;
	}

	public static HitScan ShootBullet(StateInGame game, entityBase launcher, float Range, float Angle,float Accuracy, float Radius,int MaxTargets,float FixedMinDistance)
	{
		HitScan bullet = HitScan.FireFromAngle (game,launcher,  launcher.Position, Range, Angle +(UnityEngine.Random.value*2-1)*Accuracy,Radius,MaxTargets,true,FixedMinDistance);

		//if (bullet.eR == -1) 
		{
			entitySpecialEffect.MakeGunshot (game, bullet.GetEndPosition (true)+new Vector2(
				Mathf.Sin(bullet.eR*Mathf.PI/2)/4f,
				Mathf.Cos(bullet.eR*Mathf.PI/2)/4f
			), 2, launcher.getHeight() / 2f, "explosion_default", false, bullet.eR,Color.clear);
		}
		return bullet;
	}

	public static HitScan ShootBeam(StateInGame game, entityBase launcher, float Range, float Angle,float Accuracy, float Radius,int MaxTargets,float FixedMinDistance)
	{
		HitScan bullet = HitScan.FireFromAngle (game,launcher,  launcher.Position, Range, Angle +(UnityEngine.Random.value*2-1)*Accuracy,Radius,MaxTargets,true,FixedMinDistance);

		Vector3 Delta = new Vector3 (0, 0, launcher.Position.z + launcher.getHeight() / 2f);
		//if (bullet.eR == -1) 
		{
			entitySpecialEffect.MakeGunshot (game, bullet.GetEndPosition (true), 2, launcher.getHeight() / 2f, "explosion_default", false, bullet.eR,Color.clear);
		}

		entitySpecialEffect.MakeBeam (game, (Vector3)bullet.GetStartPosition()+Delta,(Vector3) bullet.GetEndPosition(false)+Delta, 1, "beam_test", false);

		return bullet;
	}

	public static entityProjectile ShootProjectile(StateInGame game, entityBase launcher, float Dur, float Angle,float Velocity,float Accuracy, float Radius,int MaxTargets,Color32 col)
	{
		entityProjectile bullet = entityProjectile.LaunchProjectileWithFuncs (game, launcher, 1, launcher.getProjectileLaunchPosition(),
			Dur, Velocity, Angle +(UnityEngine.Random.value*2-1)*Accuracy,Radius, "bullet_default",col,

			delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
				
				effect.Die ();
			},
			delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
				(effect as entityProjectile).explode("explosion_default",0);
			}
		);
		return bullet;
	}

	public static entityProjectile ShootRocket(StateInGame game, entityBase launcher, float Dur, float Angle,float Velocity,float Accuracy, float Radius,float explodeRadius,Color32 col)
	{
		entityProjectile bullet = entityProjectile.LaunchProjectileWithFuncs (game, launcher, 1,launcher.getProjectileLaunchPosition(), 
			Dur, Velocity, Angle +(UnityEngine.Random.value*2-1)*Accuracy,Radius, "bullet_default",col,

			delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {

				effect.Die ();
			},
			delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
				(effect as entityProjectile).explode("explosion_default",explodeRadius);

			}
		);
		return bullet;
	}

	public static entityProjectile ShootGrenade(StateInGame game, entityBase launcher, float Dur, float Angle,float Velocity,float Accuracy, float Radius,float explodeRadius,Color32 col)
	{
		entityProjectile bullet = entityProjectile.LaunchProjectileWithFuncs (game, launcher, 1, launcher.getProjectileLaunchPosition(),
			Dur, Velocity, Angle +(UnityEngine.Random.value*2-1)*Accuracy,Radius, "bullet_default",col,

			delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
				//LaunchData.Die ();
			},
			delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
				(effect as entityProjectile).explode("explosion_default",explodeRadius);

			}
		);
		bullet.Lobbed.y = 2;
		return bullet;
	}
}
