﻿using System;
using UnityEngine;
using System.Collections.Generic;


public class DrawTheWorld
{
	public static float TileSize = 5;
	public static int DefaultTileSize = 20;
	public static float WallSize = 5;

	public static void DrawSmallStage(StateInGame game,string nTileSet)
	{
		Rect pos = game.getLimits ();

		game.Colchangers = new List<SpriteRenderer> ();
		//Stage Walls
		foreach (GameObject Zim in 
			DrawBorderWalls (pos,nTileSet)) {

			game.Colchangers.Add (Zim.GetComponent<SpriteRenderer>());
		}

		//Floor Texture
		float alpha=0.85f;
		game.floor=new propertyFloor(game,(int)pos.width);

		GameObject Tile = new GameObject ();
		Tile.name = "Floor";
		SpriteRenderer SpriteDrawer = Tile.AddComponent<SpriteRenderer> ();
		Tile.GetComponent<SpriteRenderer> ().color = new Color (alpha,alpha,alpha);
		SpriteDrawer.sprite = Sprite.Create (game.floor.floorTexture, new Rect (0, 0, game.floor.floorTexture.width, game.floor.floorTexture.height), new Vector2 (0.5f, 0.5f));

		Tile.transform.position = new Vector3 (pos.center.x, pos.center.y, -1);
		Tile.transform.localScale = new Vector3 (TileSize, TileSize, 1)/propertyFloor.FloorMulti;
		Tile.transform.localRotation = Quaternion.Euler (0, 0, 0);
		game.Colchangers.Add (SpriteDrawer);
	}


	public static List<GameObject> DrawBorderWalls(Rect Limits, string Terrain){

		List<GameObject> all = new List<GameObject> ();

		all.Add (DrawEdge (Limits.xMin, Limits.yMax, 0, Terrain));
		all.Add (DrawEdge (Limits.xMin, Limits.yMin, 1, Terrain));
		all.Add (DrawEdge (Limits.xMax, Limits.yMax, 3, Terrain));
		all.Add (DrawEdge (Limits.xMax, Limits.yMin, 2, Terrain));

		all.Add (DrawEdgeTile (Limits.xMin, Limits.yMax - 1, 0, Terrain));
		all.Add (DrawEdgeTile (Limits.xMin, Limits.yMin, 1, Terrain));
		all.Add (DrawEdgeTile (Limits.xMax - 1, Limits.yMax - 1, 3, Terrain));
		all.Add (DrawEdgeTile (Limits.xMax - 1, Limits.yMin, 2, Terrain));

		for (float Zim = Limits.yMin + 1; Zim < Limits.yMax - 1; Zim++) {

			all.Add (DrawWall (Limits.xMax, Zim, 3, Terrain));
			all.Add (DrawWall (Limits.xMin - 1, Zim, 1, Terrain));

			all.Add (DrawSideTile (Limits.xMax - 1, Zim, 3, Terrain));
			all.Add (DrawSideTile (Limits.xMin, Zim, 1, Terrain));
		}
		for (float Gir = Limits.xMin + 1; Gir < Limits.xMax - 1; Gir++) {

			all.Add (DrawWall (Gir, Limits.yMax, 0, Terrain));
			all.Add (DrawWall (Gir, Limits.yMin - 1, 2, Terrain));

			all.Add (DrawSideTile (Gir, Limits.yMax - 1, 0, Terrain));
			all.Add (DrawSideTile (Gir, Limits.yMin, 2, Terrain));
		}

		for (float Zim = Limits.yMin + 1; Zim < Limits.yMax - 1; Zim++) {
			for (float Gir = Limits.xMin + 1; Gir < Limits.xMax - 1; Gir++) {

				all.Add (DrawTile (Gir, Zim, 0, Terrain));
			}
		}
		return all;
	}
	public static Texture2D LoadTex(string Location)
	{
		Texture2D tex = Resources.Load<Texture2D> (Location);

		tex.filterMode = FilterMode.Point;

		return tex;
	}

	public static GameObject DrawEdge(float iX, float iY, int r, string Terrain){

		GameObject Wall = new GameObject ();
		Wall.name = "Edge " + iX + " " + iY;
		SpriteRenderer WallSprite = Wall.AddComponent<SpriteRenderer> ();

		Texture2D Tex = LoadTex ("World/" + Terrain + "/Edge");
		WallSprite.sprite = Sprite.Create (Tex, new Rect (0, 0, Tex.width, Tex.height), new Vector2 (0.5f, 0.5f));

		Wall.transform.position = new Vector3 (iX, iY,1);
		Wall.transform.localScale = new Vector3 (TileSize*DefaultTileSize/Tex.width, TileSize*DefaultTileSize/Tex.height, 1)*2;
		Wall.transform.localRotation = Quaternion.Euler (0, 0, r*90);
		return Wall;
	}

	public static GameObject DrawWall(float iX, float iY, int r, string Terrain){

		GameObject Wall = new GameObject ();
		Wall.name = "Wall " + iX + " " + iY;
		SpriteRenderer WallSprite = Wall.AddComponent<SpriteRenderer> ();
		Texture2D Tex = LoadTex ("World/" + Terrain + "/Wall");
		WallSprite.sprite = Sprite.Create (Tex, new Rect (0, 0, Tex.width, Tex.height), new Vector2 (0.5f, 0.5f));

		Wall.transform.position = new Vector3 (iX+1/2f, iY+1/2f,1);
		Wall.transform.localScale = new Vector3 (TileSize*DefaultTileSize/Tex.width, TileSize*DefaultTileSize/Tex.height, 1);
		Wall.transform.localRotation = Quaternion.Euler (0, 0, r*90);
		return Wall;
	}

	public static GameObject DrawEdgeTile(float iX, float iY, int r, string Terrain){

		GameObject Wall = new GameObject ();
		Wall.name = "Edge_Tile " + iX + " " + iY;
		SpriteRenderer WallSprite = Wall.AddComponent<SpriteRenderer> ();
		Texture2D Tex = LoadTex ("World/" + Terrain + "/Edge_Tile");
		WallSprite.sprite = Sprite.Create (Tex, new Rect (0, 0, Tex.width, Tex.height), new Vector2 (0.5f, 0.5f));

		Wall.transform.position = new Vector3 (iX+1/2f, iY+1/2f,0);
		Wall.transform.localScale = new Vector3 (TileSize*DefaultTileSize/Tex.width, TileSize*DefaultTileSize/Tex.height, 1);
		Wall.transform.localRotation = Quaternion.Euler (0, 0, r*90);
		return Wall;
	}

	public static GameObject DrawSideTile(float iX, float iY, int r, string Terrain){

		GameObject Wall = new GameObject ();
		Wall.name = "Side_Tile " + iX + " " + iY;
		SpriteRenderer WallSprite = Wall.AddComponent<SpriteRenderer> ();
		Texture2D Tex = LoadTex ("World/" + Terrain + "/Side_Tile");
		WallSprite.sprite = Sprite.Create (Tex, new Rect (0, 0, Tex.width, Tex.height), new Vector2 (0.5f, 0.5f));

		Wall.transform.position = new Vector3 (iX+1/2f, iY+1/2f,0);
		Wall.transform.localScale = new Vector3 (TileSize*DefaultTileSize/Tex.width, TileSize*DefaultTileSize/Tex.height, 1);
		Wall.transform.localRotation = Quaternion.Euler (0, 0, r*90);
		return Wall;
	}

	public static GameObject DrawTile(float iX, float iY, int r, string Terrain){

		GameObject Wall = new GameObject ();
		Wall.name = "Tile " + iX + " " + iY;
		SpriteRenderer WallSprite = Wall.AddComponent<SpriteRenderer> ();
		Texture2D Tex = LoadTex ("World/" + Terrain + "/Tile");
		WallSprite.sprite = Sprite.Create (Tex, new Rect (0, 0, Tex.width, Tex.height), new Vector2 (0.5f, 0.5f));

		Wall.transform.position = new Vector3 (iX+1/2f, iY+1/2f,0);
		Wall.transform.localScale = new Vector3 (TileSize*DefaultTileSize/Tex.width, TileSize*DefaultTileSize/Tex.height, 1);
		Wall.transform.localRotation = Quaternion.Euler (0, 0, r*90);
		return Wall;
	}


	public static entityBase Doodad (StateInGame gData, Vector2 Pos, string nTileSet, int Variation)
	{
		entityBase Doodad = new entityBase ();
		Doodad.game = gData;
		gData.gameEntities.Add (Doodad);
		Doodad.Position = Pos+Vector2.one/2;

		Doodad.gameObject = new GameObject ();
		Doodad.gameObject.name = "Doodad";
		Doodad.gameObject.transform.localScale *= Doodad.GetSpriteScale ();

		Doodad.gameObject.AddComponent < SpriteRenderer> ();

		Doodad.DrawSprites ("World/" + nTileSet + "/Doodad"+Variation);
		Doodad.gameObject.GetComponent < SpriteRenderer> ().sprite= Doodad.SpriteData[0];

		return Doodad;
	}
}

