﻿using System;
using UnityEngine;
using System.Collections.Generic;

	public class propertyWeapon
{
	public const int angleDeltaBase=30;

	public enum WeaponType {
		Melee=0,
		PulsePistol=1,
		Rifle=2,
		Rocket=3,
		Beam=4,
		Grenade=5
	}
	public bool isBetterVersion=false; //temp

	WeaponType wType = WeaponType.PulsePistol;
	public int Clip=0;
	public void Fire (StateInGame game, entityPlayer launcher,float angle)
	{
		if (Clip > 0 || wType==WeaponType.Melee) {
			float radStart = 0;
			float radDelta = 0;
			float radAdd = angleDeltaBase * GetProjectileSplashMultiplier ();

			if (GetNumProjectiles () > 1) {
					radStart = radAdd * GetNumProjectiles () / 2;
					radDelta = radAdd;
			}

			for (int iP = 0; iP < GetNumProjectiles (); iP++) {
				entityProjectile Zim;
				switch (wType) {
				case WeaponType.Melee:
					entityProjectile.ShootBullet (game, launcher, GetRange (), angle + radStart - radDelta * iP, GetAccuracy (), GetProjectileRadius (), 100,-1);
					break;
				case WeaponType.PulsePistol:
					Zim = entityProjectile.ShootProjectile (game, launcher, GetRange () / GetProjectileVelocity (), angle + radStart - radDelta * iP, GetProjectileVelocity (), GetAccuracy (), GetProjectileRadius (), 1, launcher.squirtColor);

					/*Zim.controller.AddFunction (propertyModifier.actions.collide,delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						effect.Die ();
					});*/

					Zim.Velocity += launcher.Velocity;

					Zim.controller.AddFunction (propertyModifier.actions.expire, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						entitySpecialEffect.MakeExplosion (game,(Vector2) effect.Position - effect.Velocity.normalized * effect.Radius, 0, 0, "explosion_default", false,-1,effect.squirtColor);

						/*if (GetSplashRadius () > 0) {
							game.generatePuddle (new Vector2 (effect.Position.x, effect.Position.y), GetSplashRadius (),5, launcher.squirtColor, 0);
						}*/
					});

					break;
				case WeaponType.Rifle:
					Zim = entityProjectile.ShootProjectile (game, launcher, GetRange () / GetProjectileVelocity (), angle + radStart - radDelta * iP, GetProjectileVelocity (), GetAccuracy (), GetProjectileRadius (), 1, launcher.squirtColor);

					Zim.controller.AddFunction (propertyModifier.actions.collide, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						effect.Die ();
					});

					Zim.controller.thinkInterval = 0.02f;
					Zim.controller.AddFunction (propertyModifier.actions.max, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						if (GetSplashRadius () > 0) {
							game.generatePuddle (new Vector2 (effect.Position.x, effect.Position.y), GetSplashRadius (), launcher.squirtColor, 0);
						}
					});

					Zim.controller.AddFunction (propertyModifier.actions.expire, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						entitySpecialEffect.MakeExplosion (game,(Vector2) effect.Position - effect.Velocity.normalized * effect.Radius, 0, 0, "explosion_default", false,-1,effect.squirtColor);

					});
					break;
				case WeaponType.Rocket:
					entityProjectile.ShootRocket (game, launcher, GetRange () / GetProjectileVelocity (), angle + radStart - radDelta * iP, GetProjectileVelocity (), GetAccuracy (), GetProjectileRadius (),10,launcher.squirtColor);
					break;
				case WeaponType.Grenade:
					entityProjectile.ShootGrenade (game, launcher, GetRange ()/ GetProjectileVelocity (), angle + radStart - radDelta * iP, GetProjectileVelocity (),GetAccuracy (), GetProjectileRadius (),10,launcher.squirtColor);
					break;
				case WeaponType.Beam:
					HitScan Final = entityProjectile.ShootBeam (game, launcher, GetRange (), angle + radStart - radDelta * iP, GetAccuracy (), GetProjectileRadius (), 1, GetRange () / 3);

					break;
				}
			}
			launcher.AddModifier (game, new propertyModifier ("recoil",propertyModifier.Allignment_neutral, GetRecoilTime (), new int[]{ propertyModifier.modstate_Disarmed }), 0);
			if (!HasInfiniteAmmo()) {
				Clip--;
			}
			}
		if (Clip <= 0) {
			//Reload (launcher);
			launcher.ChangeWeapon();
		}
	}

	public void Reload(entityPlayer launcher)
	{
		propertyModifier RelMod = new propertyModifier ("reload",propertyModifier.Allignment_neutral, GetReloadTime (), new int[]{ propertyModifier.modstate_Disarmed }); 
		launcher.AddModifier (launcher.game,RelMod , 0);

		RelMod.AddFunction(propertyModifier.actions.expire,delegate {
			Clip=GetClipSize();
		});

		launcher.squirtColor = launcher.GetColor ();
	}

	public float GetNumProjectiles()
	{
		switch (wType) {
		case WeaponType.Melee:
			return 1;
		case WeaponType.PulsePistol:
			return 1*GetProjectileMultiplier();
		case WeaponType.Rifle:
			return 1*GetProjectileMultiplier();
		case WeaponType.Rocket:
			return 1*GetProjectileMultiplier();
		case WeaponType.Beam:
			return 1;
		case WeaponType.Grenade:
			return 1*GetProjectileMultiplier();
		}

		return 1;
	}

	public float GetProjectileMultiplier()
	{
		if (wType == WeaponType.PulsePistol && isBetterVersion) {
			return 6;
		}
		return 1;
	}

	public float GetRecoilTime()
	{
		switch (wType) {
		case WeaponType.Melee:
			return 1*GetRecoilMultiplier();
		case WeaponType.PulsePistol:
			return 0.02f*GetRecoilMultiplier();
		case WeaponType.Rifle:
			return 1*GetRecoilMultiplier();
		case WeaponType.Rocket:
			return 1*GetRecoilMultiplier();
		case WeaponType.Beam:
			return 0.25f*GetRecoilMultiplier();
		case WeaponType.Grenade:
			return 1*GetRecoilMultiplier();
		}

		return 1;
	}

	public float GetRecoilMultiplier()
	{
		if (wType == WeaponType.PulsePistol && isBetterVersion) {
			return 10;
		}
		return 1;
	}

	public float GetRange()
	{
		switch (wType) {
		case WeaponType.Melee:
			return 0.01f*GetRangeMultiplier();
		case WeaponType.PulsePistol:
			return 20f*GetRangeMultiplier();
		case WeaponType.Rifle:
			return 15*GetRangeMultiplier();
		case WeaponType.Rocket:
			return 10*GetRangeMultiplier();
		case WeaponType.Beam:
			return 8*GetRangeMultiplier();
		case WeaponType.Grenade:
			return 5*GetRangeMultiplier();
		}

		return 1;
	}

	public float GetRangeMultiplier()
	{
		return 1;
	}

	public float GetAccuracy()
	{
		switch (wType) {
		case WeaponType.Melee:
			return 0;
		case WeaponType.PulsePistol:
			return 2f*GetAccuracyModifier();
		case WeaponType.Rifle:
			return 1*GetAccuracyModifier();
		case WeaponType.Rocket:
			return 0;
		case WeaponType.Beam:
			return 0.8f*GetAccuracyModifier();
		case WeaponType.Grenade:
			return 1*GetAccuracyModifier();
		}

		return 1;
	}
		
	public float GetAccuracyModifier()
	{
		if (wType == WeaponType.PulsePistol && isBetterVersion) {
			return 4;
		}
		return 1;
	}

	public float GetProjectileRadius()
	{
		switch (wType) {
		case WeaponType.Melee:
			return 1;
		case WeaponType.PulsePistol:
			return .25f;
		case WeaponType.Beam:
			return 1;
		case WeaponType.Rifle:
			return 1;
		case WeaponType.Rocket:
			return 1;
		case WeaponType.Grenade:
			return 1;
		}

		return 1;
	}

	public float GetProjectileVelocity()
	{
		switch (wType) {
		case WeaponType.Melee:
			return 1*GetVelocityMultiplier();
		case WeaponType.PulsePistol:
			return 20f*GetVelocityMultiplier();
		case WeaponType.Beam:
			return 1*GetVelocityMultiplier();
		case WeaponType.Rifle:
			return 1*GetVelocityMultiplier();
		case WeaponType.Rocket:
			return 2*GetVelocityMultiplier();
		case WeaponType.Grenade:
			return 1.5f*GetVelocityMultiplier();
		}

		return 1;
	}

	public float GetVelocityMultiplier(){
		return 1;
	}

	public int GetClipSize()
	{
		switch (wType) {
		case WeaponType.Melee:
			return Mathf.RoundToInt(1*GetAmmoMultiplier());
		case WeaponType.PulsePistol:
			return  Mathf.RoundToInt(60*GetAmmoMultiplier());
		case WeaponType.Beam:
			return  Mathf.RoundToInt(50*GetAmmoMultiplier());
		case WeaponType.Rifle:
			return  Mathf.RoundToInt(30*GetAmmoMultiplier());
		case WeaponType.Rocket:
			return  Mathf.RoundToInt(1*GetAmmoMultiplier());
		case WeaponType.Grenade:
			return  Mathf.RoundToInt(1*GetAmmoMultiplier());
		}

		return 1;
	}

	public float GetAmmoMultiplier(){
		if (wType == WeaponType.PulsePistol && isBetterVersion) {
			return 1/10f;
		}
		return 1;
	}

	public float GetReloadTime()
	{
		switch (wType) {
		case WeaponType.Melee:
			return 2*GetReloadMultiplier();
		case WeaponType.PulsePistol:
			return 0.5f*GetReloadMultiplier();
		case WeaponType.Beam:
			return 3*GetReloadMultiplier();
		case WeaponType.Rifle:
			return 3*GetReloadMultiplier();
		case WeaponType.Rocket:
			return 4*GetReloadMultiplier();
		case WeaponType.Grenade:
			return 4*GetReloadMultiplier();
		}

		return 1;
	}

	public float GetReloadMultiplier()
	{
		return 0;//UNUSED
	}

	public float GetDamageMultiplier()
	{
		switch (wType) {
		case WeaponType.Melee:
			return 2*GetSplashMultiplier();
		case WeaponType.PulsePistol:
			return 8*GetSplashMultiplier();
		case WeaponType.Beam:
			return 3*GetSplashMultiplier();
		case WeaponType.Rifle:
			return 3*GetSplashMultiplier();
		case WeaponType.Rocket:
			return 4*GetSplashMultiplier();
		case WeaponType.Grenade:
			return 4*GetSplashMultiplier();
		}

		return 1;
	}

	public float GetSplashRadius()
	{
		switch (wType) {
		case WeaponType.Melee:
			return 2*GetSplashMultiplier();
		case WeaponType.PulsePistol:
			return 8*GetSplashMultiplier();
		case WeaponType.Beam:
			return 3*GetSplashMultiplier();
		case WeaponType.Rifle:
			return 3*GetSplashMultiplier();
		case WeaponType.Rocket:
			return 4*GetSplashMultiplier();
		case WeaponType.Grenade:
			return 4*GetSplashMultiplier();
		}

		return 1;
	}

	public float GetSplashMultiplier()
	{
		return 1f;
	}

	public float GetProjectileSplashMultiplier()
	{
		if (wType == WeaponType.PulsePistol && isBetterVersion) {
			return 0.3f;
		}
		return 1f;
	}

	public bool HasInfiniteAmmo()
	{
		return wType == WeaponType.PulsePistol && !isBetterVersion;
	}

	public static propertyWeapon Random(WeaponType gun, bool upgrade){
		propertyWeapon Gun = new propertyWeapon ();

		Gun.wType = gun;
		Gun.isBetterVersion = upgrade;
		Gun.Clip = Gun.GetClipSize (); 

		return Gun;
	}
	}


